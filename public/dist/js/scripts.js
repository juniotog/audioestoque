$(document).ready(function(){

    /**
     * Clientes
     */
     $("#pessoa_tipo_id").change(function() {
        var value = $(this).val();

        if(value == 1) {
            $(".hidden-juridica").hide();
            $(".hidden-fisica").show();
            $("#regime_tributario_id").prop("disabled", true);
        } else {
            $(".hidden-juridica").show();
            $(".hidden-fisica").hide();
            $("#regime_tributario_id").prop("disabled", false);
        }
     })


     $(".selecionar-tudo").click(function(e) {
        var table = $(this).closest('table');
        $('td input:checkbox',table).prop('checked',e.target.checked);

     });







    $('.cpf-mask').inputmask({"mask": "999.999.999-99"});  //static mask
    $('.cnpj-mask').inputmask("99.999.999/9999-99");
    $('.telefone-mask').inputmask("(99) 9999-9999[9]");
    $('.cep-mask').inputmask("99.999-999");

    $('.delete-data').click(function() {
        if (confirm('Tem certeza que deseja deletar?')) {
            $(this).parent().parent().children('.url-form').submit();
        }
    });

    function limpa_formulário_cep() {
        $("#numero").val("");
        $("#complemento").val("");
        $("#rua").val("");
        $("#bairro").val("");
        $("#estado").val("");
        $("#cidade_id").val("");
    }

    $(".hidden-a3").hide();
    $(".show-a3").hide();

    $("#certificado_tipo_id").change(function() {
        var tipoCertificado = $(this).val();

        if (tipoCertificado == 1) {
            $(".hidden-a3").show();
            $(".show-a3").hide();
        } else {
            $(".hidden-a3").hide();
            $(".show-a3").show();
        }
    });

    $("#estado").change(function() {
        if($(this).prop('disabled') == false) {
            var estadoId = $(this).val();
            $("#cidade_id").find('option').remove();

            $.getJSON("/ajax/cidades/"+estadoId, function(dados) {
                for (i = 0; i < dados.length; i++) {
                    $('#cidade_id').append($('<option>', {
                        value: dados[i].id,
                        text: dados[i].nome
                    }));
                }
                $("#cidade_id").attr('readonly', false);
            });
        }
    });

    $("#cep").blur(function() {
        var cep = $(this).val().replace(/\D/g, '');

        if (cep != "" && cep.length == 8) {
            limpa_formulário_cep();
            $(".overlay").show();
            $("#rua").val("...");
            $("#bairro").val("...");
            $("#estado").prop('disabled', true);

            $.getJSON("//api.postmon.com.br/v1/cep/"+cep, function(dados) {
                $("#rua").val(dados.logradouro);
                $("#bairro").val(dados.bairro);
                $("#estado").val(dados.estado_info.codigo_ibge);

                if (typeof dados.cidade !== "undefined") {
                    $('#cidade_id').append($('<option>', {
                        value: dados.cidade_info.codigo_ibge,
                        text: dados.cidade
                    }));
                    $('#cidade_id').val(dados.cidade_info.codigo_ibge);
                }



                $("#numero").focus();
                $(".overlay").hide();
            }).fail(function() {
                $("#rua").prop('readonly', false);
                $("#bairro").prop('readonly', false);
                $("#estado").prop('disabled', false);
                $("#cidade_id").prop('readonly', false);
                limpa_formulário_cep();
                $("#rua").focus();

                $(".overlay").hide();
            });
        }

    });


//http://api.postmon.com.br/v1/cep/30668635

});