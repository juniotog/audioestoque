<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('movimento_tipo_id') ? ' has-error' : '' }}">
            <label>Movimento Tipo</label>
            <select class="form-control" name="movimento_tipo_id" id="movimento_tipo_id">
                @foreach ($movimento_tipos as $movimento_tipo)
                <option value="{{$movimento_tipo->id}}" @if (old('movimento_tipo_id',  isset($naturezaOperacao->movimento_tipo_id) ? $naturezaOperacao->movimento_tipo_id : null) == $movimento_tipo->id) {{"selected"}} @endif>{{$movimento_tipo->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('movimento_tipo_id'))
            <span class="help-block">
            <strong>{{ $errors->first('movimento_tipo_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Descrição</label>
            <input type="text" class="form-control" maxlength="45" value="{{ old('descricao',  isset($naturezaOperacao->descricao) ? $naturezaOperacao->descricao : null) }}" name="descricao" id="descricao" placeholder="Descrição">
            @if ($errors->has('descricao'))
            <span class="help-block">
            <strong>{{ $errors->first('descricao') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>