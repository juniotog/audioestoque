<h4>Dados principais</h4>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CNPJ</label>
            <input type="text" class="form-control cnpj-mask" value="{{ old('cnpj',  isset($cliente->cnpj) ? $cliente->cnpj : null) }}" name="cnpj" id="cnpj" placeholder="CNPJ">
            @if ($errors->has('cnpj'))
            <span class="help-block">
            <strong>{{ $errors->first('cnpj') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Razão social</label>
            <input type="text" class="form-control" value="{{ old('razao_social',  isset($cliente->razao_social) ? $cliente->razao_social : null) }}" name="razao_social" id="razao_social" placeholder="Razão social">
            @if ($errors->has('razao_social'))
            <span class="help-block">
            <strong>{{ $errors->first('razao_social') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('fantasia') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Fantasia</label>
            <input type="text" class="form-control" value="{{ old('fantasia',  isset($cliente->fantasia) ? $cliente->fantasia : null) }}" name="fantasia" id="fantasia" placeholder="Fantasia">
            @if ($errors->has('fantasia'))
            <span class="help-block">
            <strong>{{ $errors->first('fantasia') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('inscricao_estadual') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Inscrição estadual</label>
            <input type="text" class="form-control" value="{{ old('inscricao_estadual',  isset($cliente->inscricao_estadual) ? $cliente->inscricao_estadual : null) }}" name="inscricao_estadual" id="inscricao_estadual" placeholder="Inscrição estadual">
            @if ($errors->has('inscricao_estadual'))
            <span class="help-block">
            <strong>{{ $errors->first('inscricao_estadual') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('inscricao_municipal') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Inscrição municipal</label>
            <input type="text" class="form-control" value="{{ old('inscricao_municipal',  isset($cliente->inscricao_municipal) ? $cliente->inscricao_municipal : null) }}" name="inscricao_municipal" id="inscricao_municipal" placeholder="Inscrição municipal">
            @if ($errors->has('inscricao_municipal'))
            <span class="help-block">
            <strong>{{ $errors->first('inscricao_municipal') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Telefone</label>
            <input type="text" class="form-control telefone-mask" value="{{ old('telefone',  isset($cliente->telefone) ? $cliente->telefone : null) }}" name="telefone" id="telefone" placeholder="Telefone">
            @if ($errors->has('telefone'))
            <span class="help-block">
            <strong>{{ $errors->first('telefone') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" value="{{ old('email',  isset($cliente->email) ? $cliente->email : null) }}" name="email" id="email" placeholder="Email">
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('email_cobranca') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email cobrança</label>
            <input type="email" class="form-control" value="{{ old('email_cobranca',  isset($cliente->email_cobranca) ? $cliente->email_cobranca : null) }}" name="email_cobranca" id="email_cobranca" placeholder="Email cobrança">
            @if ($errors->has('email_cobranca'))
            <span class="help-block">
            <strong>{{ $errors->first('email_cobranca') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('plano_id') ? ' has-error' : '' }}">
            <label>Plano</label>
            <select class="form-control" name="plano_id" id="plano_id">
                @foreach ($planos as $plano)
                <option value="{{$plano->id}}" @if(isset($cliente->plano_id) && $cliente->plano_id == $plano->id) selected="selected" @endif>{{$plano->descricao}} - R${{$plano->preco}}</option>
                @endforeach
            </select>
            @if ($errors->has('plano_id'))
            <span class="help-block">
            <strong>{{ $errors->first('plano_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    @if (isset($cliente))
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
            <label>Status</label>
            <select class="form-control" name="status_id" id="status_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($status as $statu)
                <option value="{{$statu->id}}" @if (old('status_id',  isset($cliente->status_id) ? $cliente->status_id : null) == $statu->id) {{"selected"}} @endif>{{$statu->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('status_id'))
            <span class="help-block">
            <strong>{{ $errors->first('status_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    @endif
</div>


<hr>
<h4>Endereço</h4>
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CEP</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control cep-mask" value="{{ old('cep',  isset($cliente->cep) ? $cliente->cep : null) }}" name="cep" id="cep" placeholder="CEP">
            </div>
            @if ($errors->has('cep'))
            <span class="help-block">
            <strong>{{ $errors->first('cep') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('rua') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Logradouro</label>
            <input type="text" class="form-control" readonly value="{{ old('rua',  isset($cliente->rua) ? $cliente->rua : null) }}" name="rua" id="rua" placeholder="Logradouro">
            @if ($errors->has('rua'))
            <span class="help-block">
            <strong>{{ $errors->first('rua') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Número</label>
            <input type="text" class="form-control" value="{{ old('numero',  isset($cliente->numero) ? $cliente->numero : null) }}" name="numero" id="numero" placeholder="Número">
            @if ($errors->has('numero'))
            <span class="help-block">
            <strong>{{ $errors->first('numero') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('complemento') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Complemento</label>
            <input type="text" class="form-control" value="{{ old('complemento',  isset($cliente->complemento) ? $cliente->complemento : null) }}" name="complemento" id="complemento" placeholder="Complemento">
            @if ($errors->has('complemento'))
            <span class="help-block">
            <strong>{{ $errors->first('complemento') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Bairro</label>
            <input type="text" class="form-control" readonly value="{{ old('bairro',  isset($cliente->bairro) ? $cliente->bairro : null) }}" name="bairro" id="bairro" placeholder="Bairro">
            @if ($errors->has('bairro'))
            <span class="help-block">
            <strong>{{ $errors->first('bairro') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Estado</label>
            <select class="form-control" readonly="true" name="estado" id="estado" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($estados as $estado)
                <option value="{{$estado->id}}"  @if (old('estado',  isset($cliente->cidade->estado) ? $cliente->cidade->estado->id : null) == $estado->id) {{"selected"}} @endif>{{$estado->nome}}</option>
                @endforeach
            </select>
            @if ($errors->has('estado'))
            <span class="help-block">
            <strong>{{ $errors->first('estado') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cidade_id') ? ' has-error' : '' }}">
            <label>Cidade</label>
            <select class="form-control" readonly="true" name="cidade_id" id="cidade_id" >
                <option value="" @if (!isset($cliente->cidade)) selected="" @endif disabled=""> Selecione...</option>
                @if (isset($cliente->cidade))
                    <option value="{{$cliente->cidade->id}}" selected>{{$cliente->cidade->nome}}</option>
                @endif
            </select>
            @if ($errors->has('cidade_id'))
            <span class="help-block">
            <strong>{{ $errors->first('cidade_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>