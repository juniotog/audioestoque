@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Clientes
        <small>Visualizar cliente</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Visualizar cliente</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <dl>
                        <dt>CNPJ</dt>
                        <dd>{{ $cliente->cnpj }}</dd>

                        <dt>Razão Social</dt>
                        <dd>{{ $cliente->razao_social }}</dd>

                        <dt>Fantasia</dt>
                        <dd>{{ $cliente->fantasia }}</dd>

                        <dt>Inscrição estadual</dt>
                        <dd>{{ $cliente->inscricao_estadual }}</dd>

                        <dt>Inscrição municipal</dt>
                        <dd>{{ $cliente->inscricao_municipal }}</dd>
                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Telefone</dt>
                        <dd>{{ $cliente->telefone }}</dd>

                        <dt>CEP</dt>
                        <dd>{{ $cliente->cep }}</dd>

                        <dt>Rua</dt>
                        <dd>{{ $cliente->rua }}</dd>

                        <dt>Número</dt>
                        <dd>{{ $cliente->numero }}</dd>

                        <dt>Complemento</dt>
                        <dd>{{ $cliente->complemento }}</dd>

                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Bairro</dt>
                        <dd>{{ $cliente->bairro }}</dd>

                        <dt>Cidade</dt>
                        <dd>{{ $cliente->cidade->nome }}</dd>

                        <dt>Estado</dt>
                        <dd>{{ $cliente->cidade->estado->nome }}</dd>

                        <dt>Email</dt>
                        <dd>{{ $cliente->email }}</dd>

                        <dt>Status</dt>
                        <dd>{{ $cliente->status->descricao }}</dd>
                    </dl>
                </div>

                <div class="col-md-3">
                    <dl>
                        <dt>Criado em:</dt>
                        <dd>{{ $cliente->created_at->format('d/m/Y') }}</dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

</section>
<!-- /.content -->
@endsection