@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Clientes
        <small>Listagem de clientes</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    @include('includes.messages')

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Pesquisar</h3>
            <div class="box-tools">
                <a href="{{ route('clientes.create') }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-plus"></i> Adicionar cliente</a>
            </div>
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <form method="GET">
                        <div class="input-group margin">
                            <input type="text" class="form-control" value="{{Request::input('search')}}" name="search" placeholder="Digite o conteudo que deseja buscar">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i> Pesquisa</button>
                                </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list"></i> Listagem</h3>
        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped table-hover">
                <tbody>
                    <tr>
                        <th>Razão Social / Nome</th>
                        <th>CNPJ / CPF</th>
                        <th>Telefone</th>
                        <th>Email cobrança</th>
                        <th>Status</th>
                        <th>Usuários</th>
                        <th>Ações</th>
                    </tr>

                    @foreach($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->razao_social }}</td>
                        <td>{{ $cliente->cnpj }}</td>
                        <td>{{ $cliente->telefone }}</td>
                        <td>{{ $cliente->email_cobranca }}</td>
                        <td>{{ $cliente->status->descricao }}</td>
                        <td>{{ $cliente->users()->count() }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('clientes.show', $cliente->id) }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-eye"></i></a>
                                <a href="{{ route('clientes.edit', $cliente->id) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="submit" class="btn btn-default btn-sm delete-data"><i class="fa fa-trash"></i></button>

                            </div>

                            <form class="url-form" role="form" method="POST" action="{{ route('clientes.destroy', $cliente->id) }}">
                                <input type="hidden" name="_method" value="delete">
                                {{ csrf_field() }}
                            </form>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <p class="no-margin pull-left">Foram encontrado {{$clientes->total()}} registro(s).</p>
            {{ $clientes->links('includes.pagination') }}
        </div>
    </div>
</section>
<!-- /.content -->
@endsection