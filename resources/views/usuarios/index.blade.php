@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuários
        <small>Listagem de usuários</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->

    @include('includes.messages')

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Pesquisar</h3>
            <div class="box-tools">
                <a href="{{ route('usuarios.create') }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-plus"></i> Adicionar usuário</a>
            </div>
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <form method="GET">
                        <div class="input-group margin">
                            <input type="text" class="form-control" value="{{Request::input('search')}}" name="search" placeholder="Digite o conteudo que deseja buscar">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i> Pesquisa</button>
                                </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list"></i> Listagem</h3>
        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped table-hover">
                <tbody>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Empresa</th>
                        <th>Ações</th>
                    </tr>

                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->cpf }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->status->descricao }}</td>
                        <td>{{ $user->cliente->cnpj }} - {{ $user->cliente->razao_social }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('usuarios.show', $user->id) }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-eye"></i></a>
                                <a href="{{ route('usuarios.edit', $user->id) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="submit" class="btn btn-default btn-sm delete-data"><i class="fa fa-trash"></i></button>

                            </div>

                            <form class="url-form" role="form" method="POST" action="{{ route('usuarios.destroy', $user->id) }}">
                                <input type="hidden" name="_method" value="delete">
                                {{ csrf_field() }}
                            </form>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <p class="no-margin pull-left">Foram encontrado {{$users->total()}} registro(s).</p>
            {{ $users->links('includes.pagination') }}
        </div>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection