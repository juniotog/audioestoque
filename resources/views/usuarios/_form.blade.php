<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Nome</label>
            <input type="text" class="form-control" value="{{ old('name',  isset($user->name) ? $user->name : null) }}" name="name" id="name" placeholder="Nome">
            @if ($errors->has('name'))
            <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" value="{{ old('email',  isset($user->email) ? $user->email : null) }}" name="email" id="email" placeholder="E-Mail">
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Senha</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Senha">
            @if ($errors->has('password'))
            <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Confirmar senha</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirmar senha">
            @if ($errors->has('password_confirmation'))
            <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CPF</label>
            <input type="text" class="form-control cpf-mask" value="{{ old('cpf',  isset($user->cpf) ? $user->cpf : null) }}" name="cpf" id="cpf" placeholder="CPF">
            @if ($errors->has('cpf'))
            <span class="help-block">
            <strong>{{ $errors->first('cpf') }}</strong>
            </span>
            @endif
        </div>
    </div>

    @if(Request::segment(2) !== "funcionarios")
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cliente_id') ? ' has-error' : '' }}">
            <label>Cliente</label>
            <select class="form-control" name="cliente_id" id="cliente_id">
                @foreach ($clientes as $cliente)
                <option value="{{$cliente->id}}" @if(isset($user->cliente_id) && $user->cliente_id == $cliente->id) selected="selected" @endif>{{$cliente->cnpj}} - {{$cliente->razao_social}}</option>
                @endforeach
            </select>
            @if ($errors->has('cliente_id'))
            <span class="help-block">
            <strong>{{ $errors->first('cliente_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    @else
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('empresa_id') ? ' has-error' : '' }}">
            <label>Empresa</label>
            <select class="form-control" name="empresa_id" id="empresa_id">
                @foreach ($empresas as $empresa)
                <option value="{{$empresa->id}}" @if(isset($user->empresa_id) && $user->empresa_id == $empresa->id) selected="selected" @endif>{{$empresa->razao_social}} - {{$empresa->cnpj}}</option>
                @endforeach
            </select>
            @if ($errors->has('empresa_id'))
            <span class="help-block">
            <strong>{{ $errors->first('empresa_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    @endif

    @if (isset($user))
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
            <label>Status</label>
            <select class="form-control" name="status_id" id="status_id">
                @foreach ($status as $statu)
                <option value="{{$statu->id}}" @if(isset($user->status_id) && $user->status_id == $statu->id) selected="selected" @endif>{{$statu->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('status_id'))
            <span class="help-block">
            <strong>{{ $errors->first('status_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    @endif
</div>