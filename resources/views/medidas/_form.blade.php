<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Descrição</label>
            <input type="text" class="form-control" maxlength="45" value="{{ old('descricao',  isset($medida->descricao) ? $medida->descricao : null) }}" name="descricao" id="descricao" placeholder="Descrição">
            @if ($errors->has('descricao'))
            <span class="help-block">
            <strong>{{ $errors->first('descricao') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('abreviacao') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Abreviação</label>
            <input type="text" class="form-control" maxlength="3" value="{{ old('abreviacao',  isset($medida->abreviacao) ? $medida->abreviacao : null) }}" name="abreviacao" id="abreviacao" placeholder="Abreviação">
            @if ($errors->has('abreviacao'))
            <span class="help-block">
            <strong>{{ $errors->first('abreviacao') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>