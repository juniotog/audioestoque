@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Funcionários
        <small>Visualizar funcionário</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Visualizar funcionário</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <dl>
                        <dt>Nome</dt>
                        <dd>{{ $user->name }}</dd>

                        <dt>Email</dt>
                        <dd>{{ $user->email }}</dd>

                        <dt>CPF</dt>
                        <dd>{{ $user->cpf }}</dd>

                        <dt>Empresa</dt>
                        <dd>{{ $user->cliente->cnpj }} - {{ $user->cliente->razao_social }}</dd>

                        <dt>Criado em</dt>
                        <dd>{{ $user->created_at->format('d/m/Y') }}</dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

</section>
<!-- /.content -->
@endsection