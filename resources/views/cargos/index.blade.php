@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cargos
        <small>Listagem de cargos</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    @include('includes.messages')

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Pesquisar</h3>
            <div class="box-tools">
                <a href="{{ route('cargos.create') }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-plus"></i> Adicionar cargo</a>
            </div>
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <form method="GET">
                        <div class="input-group margin">
                            <input type="text" class="form-control" value="{{Request::input('search')}}" name="search" placeholder="Digite o conteudo que deseja buscar">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i> Pesquisa</button>
                                </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list"></i> Listagem</h3>
        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped table-hover">
                <tbody>
                    <tr>
                        <th>#</th>
                        <th>Função</th>
                        <th>Ações</th>
                    </tr>
                   
                    @foreach($cargos as $cargo)
                    <tr>
                        <td>{{ $cargo->id }}</td>
                        <td>{{ $cargo->descricao }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('cargos.show', $cargo->id) }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-eye"></i></a>
                                <a href="{{ route('cargos.edit', $cargo->id) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="submit" class="btn btn-default btn-sm delete-data"><i class="fa fa-trash"></i></button>

                            </div>

                            <form class="url-form" role="form" method="POST" action="{{ route('cargos.destroy', $cargo->id) }}">
                                <input type="hidden" name="_method" value="delete">
                                {{ csrf_field() }}
                            </form>

                        </td>
                    </tr>
                    @endforeach
                   
                </tbody>
            </table>
        </div>
        {{--<div class="box-footer clearfix">
            <p class="no-margin pull-left">Foram encontrado {{$cargos->total()}} registro(s).</p>
            {{ $cargos->links('includes.pagination') }}
        </div>--}}
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection