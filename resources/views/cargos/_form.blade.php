<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#dados_principais" data-toggle="tab">Dados principais</a></li>
        <li><a href="#permissoes" data-toggle="tab">Permissões</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="dados_principais">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                        <label for="exampleInputEmail1">Nome cargo</label>
                        <input type="text" class="form-control" value="{{ old('descricao',  isset($cargo->descricao) ? $cargo->descricao : null) }}" name="descricao" id="descricao" placeholder="Nome cargo">
                        @if ($errors->has('descricao'))
                        <span class="help-block">
                        <strong>{{ $errors->first('descricao') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="permissoes">
            @if ($errors->has('permissoes.0'))
                <div class="row">
                    <div class="col-md-12{{ $errors->has('permissoes.0') ? ' has-error' : '' }}">
                        <span class="help-block">
                        <strong>{{ $errors->first('permissoes.0') }}</strong>
                        </span>
                    </div>
                </div>
            @endif
            <div class="row">
                
                @foreach($menuPermissoes as $menu)
                    @if (count($menu['permissao']) > 0) 
                    <div class="col-md-3">
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr>
                                    <th>{{$menu['descricao']}}</th>
                                    <th><input type="checkbox" class="checkbox-permission selecionar-tudo"></th>
                            
                                </tr>
                                @foreach($menu['permissao'] as $permissao)
                                <tr>
                                    <td>{{$permissao['label']}}</td>
                                    <td><input type="checkbox" class="checkbox-permission" name="permissoes[]" value="{{$permissao['id']}}"></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@if($errors->has('permissoes.0') == true AND $errors->has('descricao') == false)
    

@push('scripts')
<script type="text/javascript">
    $("a[href='#permissoes']").click();
</script>
@endpush


@endif