@extends('layouts.login')

@section('content')


@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<p class="login-box-msg">Digite seu email para recuperar sua conta</p>
<form role="form" method="POST" action="{{ route('password.email') }}">
{{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
        <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" name="email" id="email" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>


    <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar link de redefinição de senha</button>
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
            <br>
            <a href="{{ route('login') }}" class="text-center">Voltar</a>
        </div>
    </div>
</form>
@endsection
