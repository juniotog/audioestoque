<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Descrição</label>
            <input type="text" class="form-control" maxlength="45" value="{{ old('descricao',  isset($rotulo->descricao) ? $rotulo->descricao : null) }}" name="descricao" id="descricao" placeholder="Descrição">
            @if ($errors->has('descricao'))
            <span class="help-block">
            <strong>{{ $errors->first('descricao') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>