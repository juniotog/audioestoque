@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Clientes / Fornecedores
        <small>Editar cliente/fornecedor</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Your Page Content Here -->
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Editar cliente/fornecedor</h3>
        </div>
        <form role="form" method="POST" action="{{ route('fornecedores.update', $cliente->id) }}" novalidate>
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="box-body">
                @include('fornecedores._form')
            </div>
            <div class="box-footer">
                <div class="btn-toolbar">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                    <button type="reset" class="btn btn-default">Limpar</button>
                </div>

            </div>
        </form>
        <!-- /.box-body -->
    </div>
</section>
<!-- /.content -->
@endsection