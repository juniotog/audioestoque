@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Clientes / Fornecedores
        <small>Listagem de cliente/fornecedore</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    @include('includes.messages')

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Pesquisar</h3>
            <div class="box-tools">
                <a href="{{ route('fornecedores.create') }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-plus"></i> Adicionar cliente</a>
            </div>
        </div>
        <div class="box-body">
            <form method="GET">
                <div class="row margin">
                    <div class="col-md-7">

                        <!-- <div class="input-group margin"> -->
                            <input type="text" class="form-control" value="{{Request::input('search')}}" name="search" placeholder="Digite o conteudo que deseja buscar">
                                <!-- <span class="input-group-btn">

                                </span> -->
                        <!-- </div> -->

                    </div>
                    <div class="col-md-3">
                        <select class="form-control" name="tipo_id" id="tipo_id" >
                            @foreach ($tipos as $tipo)
                            <option value="{{$tipo->id}}" @if (Request::input('tipo_id') == $tipo->id) {{"selected"}} @endif>{{$tipo->descricao}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-info btn-block btn-flat"><i class="fa fa-search"></i> Pesquisa</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list"></i> Listagem</h3>
        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped table-hover">
                <tbody>
                    <tr>
                        <th>Razão Social / Nome</th>
                        <th>CNPJ / CPF</th>
                        <th>Telefone</th>
                        <th>Tipo</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>

                    @foreach($clientes as $cliente)
                    <tr>
                        <td>{{ ($cliente->pessoa_tipo_id == 2 ? $cliente->razao_social : $cliente->nome) }}</td>
                        <td>{{ ($cliente->pessoa_tipo_id == 2 ? $cliente->cnpj : $cliente->cpf) }}</td>
                        <td>{{ $cliente->telefone }}</td>
                        <td>{{ $cliente->pessoa_tipo->descricao }}</td>
                        <td>{{ $cliente->status->descricao }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('fornecedores.show', $cliente->id) }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-eye"></i></a>
                                <a href="{{ route('fornecedores.edit', $cliente->id) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="submit" class="btn btn-default btn-sm delete-data"><i class="fa fa-trash"></i></button>

                            </div>

                            <form class="url-form" role="form" method="POST" action="{{ route('fornecedores.destroy', $cliente->id) }}">
                                <input type="hidden" name="_method" value="delete">
                                {{ csrf_field() }}
                            </form>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <p class="no-margin pull-left">Foram encontrado {{$clientes->total()}} registro(s).</p>
            {{ $clientes->links('includes.pagination') }}
        </div>
    </div>
</section>
<!-- /.content -->
@endsection