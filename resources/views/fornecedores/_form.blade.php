<h4>Dados principais</h4>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('pessoa_tipo_id') ? ' has-error' : '' }}">
            <label>Pessoa tipo</label>
            <select class="form-control" name="pessoa_tipo_id" id="pessoa_tipo_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($pessoa_tipo as $pes_tipo)
                <option value="{{$pes_tipo->id}}" @if (old('pessoa_tipo_id',  isset($cliente->pessoa_tipo_id) ? $cliente->pessoa_tipo_id : null) == $pes_tipo->id) {{"selected"}} @endif>{{$pes_tipo->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('pessoa_tipo_id'))
            <span class="help-block">
            <strong>{{ $errors->first('pessoa_tipo_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3 hidden-juridica">
        <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CNPJ</label>
            <input type="text" class="form-control cnpj-mask" value="{{ old('cnpj',  isset($cliente->cnpj) ? $cliente->cnpj : null) }}" name="cnpj" id="cnpj" placeholder="CNPJ">
            @if ($errors->has('cnpj'))
            <span class="help-block">
            <strong>{{ $errors->first('cnpj') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3 hidden-juridica">
        <div class="form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Razão social</label>
            <input type="text" class="form-control" value="{{ old('razao_social',  isset($cliente->razao_social) ? $cliente->razao_social : null) }}" name="razao_social" id="razao_social" placeholder="Razão social">
            @if ($errors->has('razao_social'))
            <span class="help-block">
            <strong>{{ $errors->first('razao_social') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3 hidden-juridica">
        <div class="form-group{{ $errors->has('fantasia') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Fantasia</label>
            <input type="text" class="form-control" value="{{ old('fantasia',  isset($cliente->fantasia) ? $cliente->fantasia : null) }}" name="fantasia" id="fantasia" placeholder="Fantasia">
            @if ($errors->has('fantasia'))
            <span class="help-block">
            <strong>{{ $errors->first('fantasia') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3 hidden-fisica">
        <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CPF</label>
            <input type="text" class="form-control cpf-mask" value="{{ old('cpf',  isset($cliente->cpf) ? $cliente->cpf : null) }}" name="cpf" id="cpf" placeholder="CPF">
            @if ($errors->has('cpf'))
            <span class="help-block">
            <strong>{{ $errors->first('cpf') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3 hidden-fisica">
        <div class="form-group{{ $errors->has('rg') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">RG</label>
            <input type="text" class="form-control" value="{{ old('rg',  isset($cliente->rg) ? $cliente->rg : null) }}" name="rg" id="rg" placeholder="RG">
            @if ($errors->has('rg'))
            <span class="help-block">
            <strong>{{ $errors->first('rg') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3 hidden-fisica">
        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Nome</label>
            <input type="text" class="form-control" value="{{ old('nome',  isset($cliente->nome) ? $cliente->nome : null) }}" name="nome" id="nome" placeholder="Nome">
            @if ($errors->has('nome'))
            <span class="help-block">
            <strong>{{ $errors->first('nome') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 hidden-juridica">
        <div class="form-group{{ $errors->has('inscricao_estadual') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Inscrição estadual</label>
            <input type="text" class="form-control" value="{{ old('inscricao_estadual',  isset($cliente->inscricao_estadual) ? $cliente->inscricao_estadual : null) }}" name="inscricao_estadual" id="inscricao_estadual" placeholder="Inscrição estadual">
            @if ($errors->has('inscricao_estadual'))
            <span class="help-block">
            <strong>{{ $errors->first('inscricao_estadual') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3 hidden-juridica">
        <div class="form-group{{ $errors->has('inscricao_municipal') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Inscrição municipal</label>
            <input type="text" class="form-control" value="{{ old('inscricao_municipal',  isset($cliente->inscricao_municipal) ? $cliente->inscricao_municipal : null) }}" name="inscricao_municipal" id="inscricao_municipal" placeholder="Inscrição municipal">
            @if ($errors->has('inscricao_municipal'))
            <span class="help-block">
            <strong>{{ $errors->first('inscricao_municipal') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3 hidden-fisica">
        <div class="form-group{{ $errors->has('sexo_id') ? ' has-error' : '' }}">
            <label>Sexo</label>
            <select class="form-control" name="sexo_id" id="sexo_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($sexo as $sex)
                <option value="{{$sex->id}}" @if (old('sexo_id',  isset($cliente->sexo_id) ? $cliente->sexo_id : null) == $sex->id) {{"selected"}} @endif>{{$sex->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('sexo_id'))
            <span class="help-block">
            <strong>{{ $errors->first('sexo_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('contribuinte_id') ? ' has-error' : '' }}">
            <label>Contribuinte</label>
            <select class="form-control" name="contribuinte_id" id="contribuinte_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($contribuinte as $contr)
                <option value="{{$contr->id}}" @if (old('contribuinte_id',  isset($cliente->contribuinte_id) ? $cliente->contribuinte_id : null) == $contr->id) {{"selected"}} @endif>{{$contr->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('contribuinte_id'))
            <span class="help-block">
            <strong>{{ $errors->first('contribuinte_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('regime_tributario_id') ? ' has-error' : '' }}">
            <label>Regime Tributario</label>
            <select class="form-control" name="regime_tributario_id" id="regime_tributario_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($regime_tributario as $regime)
                <option value="{{$regime->id}}" @if (old('regime_tributario_id',  isset($cliente->regime_tributario_id) ? $cliente->regime_tributario_id : null) == $regime->id) {{"selected"}} @endif>{{$regime->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('regime_tributario_id'))
            <span class="help-block">
            <strong>{{ $errors->first('regime_tributario_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Telefone</label>
            <input type="text" class="form-control telefone-mask" value="{{ old('telefone',  isset($cliente->telefone) ? $cliente->telefone : null) }}" name="telefone" id="telefone" placeholder="Telefone">
            @if ($errors->has('telefone'))
            <span class="help-block">
            <strong>{{ $errors->first('telefone') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" value="{{ old('email',  isset($cliente->email) ? $cliente->email : null) }}" name="email" id="email" placeholder="Email">
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('limite') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Limite</label>
            <input type="text" class="form-control" value="{{ old('limite',  isset($cliente->limite) ? $cliente->limite : null) }}" name="limite" id="limite" placeholder="Limite">
            @if ($errors->has('limite'))
            <span class="help-block">
            <strong>{{ $errors->first('limite') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('tipo_id') ? ' has-error' : '' }}">
            <label>Tipo</label>
            <select class="form-control" name="tipo_id" id="tipo_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($tipos as $tipo)
                <option value="{{$tipo->id}}" @if (old('tipo_id',  isset($cliente->tipo_id) ? $cliente->tipo_id : null) == $tipo->id) {{"selected"}} @endif>{{$tipo->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('tipo_id'))
            <span class="help-block">
            <strong>{{ $errors->first('tipo_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
{{--<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('tipo_id') ? ' has-error' : '' }}">
            <label>Tipo</label>
            <select class="form-control" name="tipo_id" id="tipo_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($tipos as $tipo)
                <option value="{{$tipo->id}}" @if (old('tipo_id',  isset($cliente->tipo_id) ? $cliente->tipo_id : null) == $tipo->id) {{"selected"}} @endif>{{$tipo->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('tipo_id'))
            <span class="help-block">
            <strong>{{ $errors->first('tipo_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>--}}

<hr>
<h4>Endereço</h4>
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CEP</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control cep-mask" value="{{ old('cep',  isset($cliente->cep) ? $cliente->cep : null) }}" name="cep" id="cep" placeholder="CEP">
            </div>
            @if ($errors->has('cep'))
            <span class="help-block">
            <strong>{{ $errors->first('cep') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('rua') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Logradouro</label>
            <input type="text" class="form-control" readonly value="{{ old('rua',  isset($cliente->rua) ? $cliente->rua : null) }}" name="rua" id="rua" placeholder="Logradouro">
            @if ($errors->has('rua'))
            <span class="help-block">
            <strong>{{ $errors->first('rua') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Número</label>
            <input type="text" class="form-control" value="{{ old('numero',  isset($cliente->numero) ? $cliente->numero : null) }}" name="numero" id="numero" placeholder="Número">
            @if ($errors->has('numero'))
            <span class="help-block">
            <strong>{{ $errors->first('numero') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('complemento') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Complemento</label>
            <input type="text" class="form-control" value="{{ old('complemento',  isset($cliente->complemento) ? $cliente->complemento : null) }}" name="complemento" id="complemento" placeholder="Complemento">
            @if ($errors->has('complemento'))
            <span class="help-block">
            <strong>{{ $errors->first('complemento') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Bairro</label>
            <input type="text" class="form-control" readonly value="{{ old('bairro',  isset($cliente->bairro) ? $cliente->bairro : null) }}" name="bairro" id="bairro" placeholder="Bairro">
            @if ($errors->has('bairro'))
            <span class="help-block">
            <strong>{{ $errors->first('bairro') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Estado</label>
            <select class="form-control" readonly="true" name="estado" id="estado" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($estados as $estado)
                <option value="{{$estado->id}}"  @if (old('estado',  isset($cliente->cidade->estado) ? $cliente->cidade->estado->id : null) == $estado->id) {{"selected"}} @endif>{{$estado->nome}}</option>
                @endforeach
            </select>
            @if ($errors->has('estado'))
            <span class="help-block">
            <strong>{{ $errors->first('estado') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cidade_id') ? ' has-error' : '' }}">
            <label>Cidade</label>
            <select class="form-control" readonly="true" name="cidade_id" id="cidade_id" >
                <option value="" @if (!isset($cliente->cidade)) selected="" @endif disabled=""> Selecione...</option>
                @if (isset($cliente->cidade))
                    <option value="{{$cliente->cidade->id}}" selected>{{$cliente->cidade->nome}}</option>
                @endif
            </select>
            @if ($errors->has('cidade_id'))
            <span class="help-block">
            <strong>{{ $errors->first('cidade_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

<hr>
<h4>Info. adicionais</h4>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('observacao') ? ' has-error' : '' }}">
            <label>Observação</label>
            <textarea name="observacao" id="observacao" class="form-control" rows="3" placeholder="Observação">{{ old('observacao',  isset($cliente->observacao) ? $cliente->observacao : null) }}</textarea>
            @if ($errors->has('observacao'))
            <span class="help-block">
                <strong>{{ $errors->first('observacao') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('observacao_nfe') ? ' has-error' : '' }}">
            <label>Observação NFe</label>
            <textarea name="observacao_nfe" id="observacao_nfe" class="form-control" rows="3" placeholder="Observação NFe">{{ old('observacao_nfe',  isset($cliente->observacao_nfe) ? $cliente->observacao_nfe : null) }}</textarea>
            @if ($errors->has('observacao_nfe'))
            <span class="help-block">
                <strong>{{ $errors->first('observacao_nfe') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

@push('scripts')
<script>
$(document).ready(function(){
    $("#pessoa_tipo_id").val(2).change();


    var num = $("#numero").val();
    $("#cep").change(function() {
        $("#numero").val(num);
    });
});
</script>
@endpush
