@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Clientes / Fornecedores
        <small>Visualizar cliente/fornecedor</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Visualizar cliente/fornecedor</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <dl>
                        <dt>Tipo usuário</dt>
                        <dd>{{ $cliente->pessoa_tipo->descricao }}</dd>

                        <dt>Tipo</dt>
                        <dd>{{ $cliente->tipo->descricao }}</dd>

                        @if ($cliente->pessoa_tipo_id == 2)
                        <dt>Regime tributário</dt>
                        <dd>{{ $cliente->regime_tributario->descricao }}</dd>

                        <dt>CNPJ</dt>
                        <dd>{{ $cliente->cnpj }}</dd>

                        <dt>Razão Social</dt>
                        <dd>{{ $cliente->razao_social }}</dd>

                        <dt>Fantasia</dt>
                        <dd>{{ $cliente->fantasia }}</dd>

                        <dt>Inscrição estadual</dt>
                        <dd>{{ $cliente->inscricao_estadual }}</dd>

                        <dt>Inscrição municipal</dt>
                        <dd>{{ $cliente->inscricao_municipal }}</dd>
                        @else
                        <dt>CPF</dt>
                        <dd>{{ $cliente->cpf }}</dd>

                        <dt>Nome</dt>
                        <dd>{{ $cliente->nome }}</dd>

                        <dt>RG</dt>
                        <dd>{{ $cliente->rg }}</dd>

                        <dt>Sexo</dt>
                        <dd>{{ $cliente->sexo->descricao }}</dd>
                        @endif
                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Telefone</dt>
                        <dd>{{ $cliente->telefone }}</dd>

                        <dt>CEP</dt>
                        <dd>{{ $cliente->cep }}</dd>

                        <dt>Rua</dt>
                        <dd>{{ $cliente->rua }}</dd>

                        <dt>Número</dt>
                        <dd>{{ $cliente->numero }}</dd>

                        <dt>Complemento</dt>
                        <dd>{{ $cliente->complemento }}</dd>

                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Bairro</dt>
                        <dd>{{ $cliente->bairro }}</dd>

                        <dt>Cidade</dt>
                        <dd>{{ $cliente->cidade->nome }}</dd>

                        <dt>Estado</dt>
                        <dd>{{ $cliente->cidade->estado->nome }}</dd>

                        <dt>Email</dt>
                        <dd>{{ $cliente->email }}</dd>

                        <dt>Limite</dt>
                        <dd>{{ $cliente->limite }}</dd>
                    </dl>
                </div>

                <div class="col-md-3">
                    <dl>
                        <dt>Status</dt>
                        <dd>{{ $cliente->status->descricao }}</dd>

                        <dt>Observação</dt>
                        <dd>{{ $cliente->observacao }}</dd>

                        <dt>Observação NFe</dt>
                        <dd>{{ $cliente->observacao_nfe }}</dd>

                        <dt>Criado em:</dt>
                        <dd>{{ $cliente->created_at->format('d/m/Y') }}</dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

</section>
<!-- /.content -->
@endsection