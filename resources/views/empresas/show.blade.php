@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Empresas
        <small>Visualizar empresa</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Visualizar empresa</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3 text-center">
                    <dl>
                        <dt>LOGO</dt>
                        <dd><img src="data:image/png;base64, {{ $empresa->logotipo }}" alt="..." height="120" width="120" class="margin"></dd>
                    </dl>

                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>CNPJ</dt>
                        <dd>{{ $empresa->cnpj }}</dd>

                        <dt>Razão Social</dt>
                        <dd>{{ $empresa->razao_social }}</dd>

                        <dt>Nome fantasia</dt>
                        <dd>{{ $empresa->fantasia }}</dd>

                        <dt>Inscrição estadual</dt>
                        <dd>{{ $empresa->inscricao_estadual }}</dd>

                        <dt>Inscrição municipal</dt>
                        <dd>{{ $empresa->inscricao_municipal }}</dd>
                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Telefone</dt>
                        <dd>{{ $empresa->telefone }}</dd>

                        <dt>CEP</dt>
                        <dd>{{ $empresa->cep }}</dd>

                        <dt>Rua</dt>
                        <dd>{{ $empresa->rua }}</dd>

                        <dt>Número</dt>
                        <dd>{{ $empresa->numero }}</dd>

                        <dt>Complemento</dt>
                        <dd>{{ $empresa->complemento }}</dd>

                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Bairro</dt>
                        <dd>{{ $empresa->bairro }}</dd>

                        <dt>Cidade</dt>
                        <dd>{{ $empresa->cidade->nome }}</dd>

                        <dt>Estado</dt>
                        <dd>{{ $empresa->cidade->estado->nome }}</dd>

                        <dt>Email</dt>
                        <dd>{{ $empresa->email }}</dd>

                        <dt>Email contador</dt>
                        <dd>{{ $empresa->email_contador }}</dd>
                    </dl>
                </div>
            </div>
            <hr>
            <h4>Tributação</h4>
            <div class="row">
                <div class="col-md-3">
                    <dl>
                        <dt>Regime tributário</dt>
                        <dd>{{ $empresa->regime_tributario->descricao }}</dd>

                        <dt>Cnae principal</dt>
                        <dd>{{ $empresa->cnae_principal->descricao }}</dd>

                        <dt>Cnae secundário</dt>
                        <dd>{{ $empresa->cnae_secundario->descricao }}</dd>

                    </dl>
                </div>

                <div class="col-md-3">
                    <dl>
                        <dt>PIS</dt>
                        <dd>{{ $empresa->pis }}</dd>

                        <dt>CONFINS</dt>
                        <dd>{{ $empresa->confis }}</dd>


                    </dl>
                </div>

                <div class="col-md-3">
                    <dl>
                        <dt>IR</dt>
                        <dd>{{ $empresa->ir }}</dd>

                        <dt>CSLL</dt>
                        <dd>{{ $empresa->csll }}</dd>


                    </dl>
                </div>

                <div class="col-md-3">
                    <dl>
                        <dt>ISS</dt>
                        <dd>{{ $empresa->iss }}</dd>

                        <dt>INSS</dt>
                        <dd>{{ $empresa->inss }}</dd>
                    </dl>
                </div>
            </div>
            <hr>
            <h4>NFe</h4>
            <div class="row">
                <div class="col-md-3">
                    <dl>
                        <dt>Serie</dt>
                        <dd>{{ $empresa->serie }}</dd>

                        <dt>Núm. homologação</dt>
                        <dd>{{ $empresa->num_hom }}</dd>
                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Série recibo</dt>
                        <dd>{{ $empresa->serie_recibo }}</dd>

                        <dt>Número recibo</dt>
                        <dd>{{ $empresa->numero_recibo }}</dd>
                    </dl>
                </div>
                <div class="col-md-3">
                    <dl>
                        <dt>Núm. Produção</dt>
                        <dd>{{ $empresa->num_prod }}</dd>
                        <dt>Status</dt>
                        <dd>{{ $empresa->status->descricao }}</dd>
                    </dl>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

</section>
<!-- /.content -->
@endsection