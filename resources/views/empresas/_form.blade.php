<h4>Dados principais</h4>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CNPJ</label>
            <input type="text" class="form-control cnpj-mask" value="{{ old('cnpj',  isset($empresa->cnpj) ? $empresa->cnpj : null) }}" name="cnpj" id="cnpj" placeholder="CNPJ">
            @if ($errors->has('cnpj'))
            <span class="help-block">
            <strong>{{ $errors->first('cnpj') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Razao social</label>
            <input type="text" class="form-control" value="{{ old('razao_social',  isset($empresa->razao_social) ? $empresa->razao_social : null) }}" name="razao_social" id="razao_social" placeholder="Razao social">
            @if ($errors->has('razao_social'))
            <span class="help-block">
            <strong>{{ $errors->first('razao_social') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('fantasia') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Fantasia</label>
            <input type="text" class="form-control" value="{{ old('fantasia',  isset($empresa->fantasia) ? $empresa->fantasia : null) }}" name="fantasia" id="fantasia" placeholder="Fantasia">
            @if ($errors->has('fantasia'))
            <span class="help-block">
            <strong>{{ $errors->first('fantasia') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('inscricao_estadual') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Inscrição estadual</label>
            <input type="text" class="form-control" value="{{ old('inscricao_estadual',  isset($empresa->inscricao_estadual) ? $empresa->inscricao_estadual : null) }}" name="inscricao_estadual" id="inscricao_estadual" placeholder="Inscrição estadual">
            @if ($errors->has('inscricao_estadual'))
            <span class="help-block">
            <strong>{{ $errors->first('inscricao_estadual') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('inscricao_municipal') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Inscrição municipal</label>
            <input type="text" class="form-control" value="{{ old('inscricao_municipal',  isset($empresa->inscricao_municipal) ? $empresa->inscricao_municipal : null) }}" name="inscricao_municipal" id="inscricao_municipal" placeholder="Inscrição municipal">
            @if ($errors->has('inscricao_municipal'))
            <span class="help-block">
            <strong>{{ $errors->first('inscricao_municipal') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Telefone</label>
            <input type="text" class="form-control telefone-mask" value="{{ old('telefone',  isset($empresa->telefone) ? $empresa->telefone : null) }}" name="telefone" id="telefone" placeholder="Telefone">
            @if ($errors->has('telefone'))
            <span class="help-block">
            <strong>{{ $errors->first('telefone') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email</label>
            <input type="text" class="form-control" value="{{ old('email',  isset($empresa->email) ? $empresa->email : null) }}" name="email" id="email" placeholder="Email">
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('email_contador') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Email contador</label>
            <input type="text" class="form-control" value="{{ old('email_contador',  isset($empresa->email_contador) ? $empresa->email_contador : null) }}" name="email_contador" id="email_contador" placeholder="Email contador">
            @if ($errors->has('email_contador'))
            <span class="help-block">
            <strong>{{ $errors->first('email_contador') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('logotipo') ? ' has-error' : '' }}">
            <label for="exampleInputFile">Logo</label>
            <input type="file" id="logotipo" name="logotipo">
            <p class="help-block">Selecione o logotipo</p>
            @if ($errors->has('logotipo'))
            <span class="help-block">
            <strong>{{ $errors->first('logotipo') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<hr>
<h4>Endereço</h4>
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CEP</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control cep-mask" value="{{ old('cep',  isset($empresa->cep) ? $empresa->cep : null) }}" name="cep" id="cep" placeholder="CEP">
            </div>
            @if ($errors->has('cep'))
            <span class="help-block">
            <strong>{{ $errors->first('cep') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('rua') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Logradouro</label>
            <input type="text" class="form-control" readonly value="{{ old('rua',  isset($empresa->rua) ? $empresa->rua : null) }}" name="rua" id="rua" placeholder="Logradouro">
            @if ($errors->has('rua'))
            <span class="help-block">
            <strong>{{ $errors->first('rua') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Número</label>
            <input type="text" class="form-control" value="{{ old('numero',  isset($empresa->numero) ? $empresa->numero : null) }}" name="numero" id="numero" placeholder="Número">
            @if ($errors->has('numero'))
            <span class="help-block">
            <strong>{{ $errors->first('numero') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('complemento') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Complemento</label>
            <input type="text" class="form-control" value="{{ old('complemento',  isset($empresa->complemento) ? $empresa->complemento : null) }}" name="complemento" id="complemento" placeholder="Complemento">
            @if ($errors->has('complemento'))
            <span class="help-block">
            <strong>{{ $errors->first('complemento') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Bairro</label>
            <input type="text" class="form-control" readonly value="{{ old('bairro',  isset($empresa->bairro) ? $empresa->bairro : null) }}" name="bairro" id="bairro" placeholder="Bairro">
            @if ($errors->has('bairro'))
            <span class="help-block">
            <strong>{{ $errors->first('bairro') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Estado</label>
            <select class="form-control" readonly="true" name="estado" id="estado" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($estados as $estado)
                <option value="{{$estado->id}}"  @if (old('estado',  isset($empresa->cidade->estado) ? $empresa->cidade->estado->id : null) == $estado->id) {{"selected"}} @endif>{{$estado->nome}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group{{ $errors->has('cidade_id') ? ' has-error' : '' }}">
            <label>Cidade</label>
            <select class="form-control" readonly="true" name="cidade_id" id="cidade_id" >
                <option value="" @if (!isset($empresa->cidade)) selected="" @endif disabled=""> Selecione...</option>
                @if (isset($empresa->cidade))
                    <option value="{{$empresa->cidade->id}}" selected>{{$empresa->cidade->nome}}</option>
                @endif
            </select>
            @if ($errors->has('cidade_id'))
            <span class="help-block">
            <strong>{{ $errors->first('cidade_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<hr>
<h4>Tributação</h4>
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('regime_tributario_id') ? ' has-error' : '' }}">
            <label>Regime tributario</label>
            <select class="form-control" name="regime_tributario_id" id="regime_tributario_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($regime_tributario as $reg_tributario)
                <option value="{{$reg_tributario->id}}" @if (old('regime_tributario_id',  isset($empresa->regime_tributario_id) ? $empresa->regime_tributario_id : null) == $reg_tributario->id) {{"selected"}} @endif>{{$reg_tributario->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('regime_tributario_id'))
            <span class="help-block">
            <strong>{{ $errors->first('regime_tributario_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
    <div class="form-group{{ $errors->has('cnae_principal_id') ? ' has-error' : '' }}">
            <label>Cnae principal</label>
            <select class="form-control" name="cnae_principal_id" id="cnae_principal_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($cnaes as $cnae)
                <option value="{{$cnae->id}}" @if (old('cnae_principal_id',  isset($empresa->cnae_principal_id) ? $empresa->cnae_principal_id : null) == $cnae->id) {{"selected"}} @endif>{{$cnae->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('cnae_principal_id'))
            <span class="help-block">
            <strong>{{ $errors->first('cnae_principal_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group{{ $errors->has('cnae_secundario_id') ? ' has-error' : '' }}">
            <label>Cnae secundário</label>
            <select class="form-control" name="cnae_secundario_id" id="cnae_secundario_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                @foreach ($cnaes as $cnae)
                <option value="{{$cnae->id}}"  @if (old('cnae_secundario_id',  isset($empresa->cnae_secundario_id) ? $empresa->cnae_secundario_id : null) == $cnae->id) {{"selected"}} @endif>{{$cnae->descricao}}</option>
                @endforeach
            </select>
            @if ($errors->has('cnae_secundario_id'))
            <span class="help-block">
            <strong>{{ $errors->first('cnae_secundario_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('ir') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Ir</label>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" class="form-control" value="{{ old('ir',  isset($empresa->ir) ? $empresa->ir : null) }}" name="ir" id="ir" placeholder="0.00">
            </div>
            @if ($errors->has('ir'))
            <span class="help-block">
            <strong>{{ $errors->first('ir') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('csll') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CSLL</label>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" class="form-control" value="{{ old('csll',  isset($empresa->csll) ? $empresa->csll : null) }}" name="csll" id="csll" placeholder="0.00">
            </div>
            @if ($errors->has('csll'))
            <span class="help-block">
            <strong>{{ $errors->first('csll') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('pis') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">PIS</label>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" class="form-control" value="{{ old('pis',  isset($empresa->pis) ? $empresa->pis : null) }}" name="pis" id="pis" placeholder="0.00">
            </div>
            @if ($errors->has('pis'))
            <span class="help-block">
            <strong>{{ $errors->first('pis') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('confis') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">CONFIS</label>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" class="form-control" value="{{ old('confis',  isset($empresa->confis) ? $empresa->confis : null) }}" name="confis" id="confis" placeholder="0.00">
            </div>
            @if ($errors->has('confis'))
            <span class="help-block">
            <strong>{{ $errors->first('confis') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('inss') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">INSS</label>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" class="form-control" value="{{ old('inss',  isset($empresa->inss) ? $empresa->inss : null) }}" name="inss" id="inss" placeholder="0.00">
            </div>
            @if ($errors->has('inss'))
            <span class="help-block">
            <strong>{{ $errors->first('inss') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('iss') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">ISS</label>
            <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" class="form-control" value="{{ old('iss',  isset($empresa->iss) ? $empresa->iss : null) }}" name="iss" id="iss" placeholder="0.00">
            </div>
            @if ($errors->has('iss'))
            <span class="help-block">
            <strong>{{ $errors->first('iss') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<hr>
<h4>Configuração NFe</h4>
<div class="row">
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('serie') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Série</label>
             <input type="text" class="form-control" value="{{ old('serie',  isset($empresa->serie) ? $empresa->serie : null) }}" name="serie" id="serie" placeholder="Série">
            @if ($errors->has('serie'))
            <span class="help-block">
            <strong>{{ $errors->first('serie') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('num_hom') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Núm. homologação</label>
             <input type="text" class="form-control" value="{{ old('num_hom',  isset($empresa->num_hom) ? $empresa->num_hom : null) }}" name="num_hom" id="num_hom" placeholder="Núm. homologação">
            @if ($errors->has('num_hom'))
            <span class="help-block">
            <strong>{{ $errors->first('num_hom') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('num_prod') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Núm. Produção</label>
             <input type="text" class="form-control" value="{{ old('num_prod',  isset($empresa->num_prod) ? $empresa->num_prod : null) }}" name="num_prod" id="num_prod" placeholder="Núm. Produção">
            @if ($errors->has('num_prod'))
            <span class="help-block">
            <strong>{{ $errors->first('num_prod') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('serie_recibo') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Série Recibo</label>
             <input type="text" class="form-control" value="{{ old('serie_recibo',  isset($empresa->serie_recibo) ? $empresa->serie_recibo : null) }}" name="serie_recibo" id="serie_recibo" placeholder="Série Recibo">
            @if ($errors->has('serie_recibo'))
            <span class="help-block">
            <strong>{{ $errors->first('serie_recibo') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('numero_recibo') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Núm. Recibo</label>
             <input type="text" class="form-control" value="{{ old('numero_recibo',  isset($empresa->numero_recibo) ? $empresa->numero_recibo : null) }}" name="numero_recibo" id="numero_recibo" placeholder="Núm. Recibo">
            @if ($errors->has('numero_recibo'))
            <span class="help-block">
            <strong>{{ $errors->first('numero_recibo') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('certificado_tipo_id') ? ' has-error' : '' }}">
            <label>Tipo certificado</label>
            <select class="form-control" name="certificado_tipo_id" id="certificado_tipo_id" >
                <option value="" selected="" disabled=""> Selecione...</option>
                <option value="1" @if (old('certificado_tipo_id',  isset($empresa->certificado_tipo_id) ? $empresa->certificado_tipo_id : null) == 1) selected @endif>A1</option>
                <option value="2" @if (old('certificado_tipo_id',  isset($empresa->certificado_tipo_id) ? $empresa->certificado_tipo_id : null) == 2) selected @endif>A3</option>
            </select>
            @if ($errors->has('certificado_tipo_id'))
            <span class="help-block">
            <strong>{{ $errors->first('certificado_tipo_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 hidden-a3">
        <div class="form-group{{ $errors->has('certificado') ? ' has-error' : '' }}">
            <label for="exampleInputFile">Certificado A1</label>
            <input type="file" id="certificado" name="certificado">
            <p class="help-block">Selecione o certificado</p>
            @if ($errors->has('certificado'))
            <span class="help-block">
            <strong>{{ $errors->first('certificado') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-3 hidden-a3">
        <div class="form-group{{ $errors->has('senha') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Senha certificado</label>
             <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha certificado">
            @if ($errors->has('senha'))
            <span class="help-block">
            <strong>{{ $errors->first('senha') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-md-6 show-a3">
        <div class="callout callout-info">
            <h4>Baixe o emissor A3</h4>
            <p>Certificado A3 requer programa emissor, clique <a href="">aqui</a> para baixar.</p>
        </div>
    </div>
</div>

@push('scripts')
<script>
$(document).ready(function(){
    $("#certificado_tipo_id").change();
});
</script>
@endpush



<div class="overlay" style="display: none;">
    <i class="fa fa-refresh fa-spin"></i>
</div>