<?php

use Illuminate\Database\Seeder;

class ContribuinteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contribuinte')->truncate();
        DB::table('contribuinte')->insert([
            [
                'id' => 1,
                'descricao' => 'Sim'
            ],
            [
                'id' => 2,
                'descricao' => 'Não'
            ]
        ]);
    }
}
