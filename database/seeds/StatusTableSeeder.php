<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->truncate();
        DB::table('status')->insert([
            [
                'id' => 1,
                'descricao' => 'Ativo'
            ],
            [
                'id' => 2,
                'descricao' => 'Bloqueado'
            ],
            [
                'id' => 3,
                'descricao' => 'Pendente'
            ],
            [
                'id' => 4,
                'descricao' => 'Inativo'
            ],
            [
                'id' => 5,
                'descricao' => 'Excluído'
            ]
        ]);
    }
}
