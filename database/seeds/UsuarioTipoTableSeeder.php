<?php

use Illuminate\Database\Seeder;

class UsuarioTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario_tipo')->truncate();
        DB::table('usuario_tipo')->insert([
            [
                'id' => 1,
                'descricao' => 'Administrador sistema'
            ],
            [
                'id' => 2,
                'descricao' => 'Administrador'
            ],
            [
                'id' => 3,
                'descricao' => 'Usuário'
            ]
        ]);
    }
}
