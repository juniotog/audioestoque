<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(PessoaTipoTableSeeder::class);
        $this->call(SexoTableSeeder::class);
        $this->call(ContribuinteTableSeeder::class);
        $this->call(TipoTableSeeder::class);
        $this->call(CnaeTableSeeder::class);
        $this->call(UsuarioTipoTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PlanosTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(PaisTableSeeder::class);
        $this->call(EstadosTableSeeder::class);
        $this->call(CidadesTableSeeder::class);
        $this->call(RegimeTributarioTableSeeder::class);
        $this->call(MovimentoTipoTableSeeder::class);
        $this->call(NcmTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}