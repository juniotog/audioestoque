<?php

use Illuminate\Database\Seeder;

class PessoaTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pessoa_tipo')->truncate();
        DB::table('pessoa_tipo')->insert([
            [
                'id' => 1,
                'descricao' => 'Pessoa física'
            ],
            [
                'id' => 2,
                'descricao' => 'Pessoa jurídica'
            ]
        ]);
    }
}
