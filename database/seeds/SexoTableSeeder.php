<?php

use Illuminate\Database\Seeder;

class SexoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sexo')->truncate();
        DB::table('sexo')->insert([
            [
                'id' => 1,
                'descricao' => 'Masculino'
            ],
            [
                'id' => 2,
                'descricao' => 'Feminino'
            ]
        ]);
    }
}
