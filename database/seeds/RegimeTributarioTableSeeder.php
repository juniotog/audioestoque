<?php

use Illuminate\Database\Seeder;

class RegimeTributarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regime_tributario')->truncate();
        DB::table('regime_tributario')->insert([
            [
                'id' => 1,
                'descricao' => 'Simples nacional'
            ],
            [
                'id' => 2,
                'descricao' => 'Simples nacional - excesso de sublimite da receita bruta'
            ],
            [
                'id' => 3,
                'descricao' => 'Regime normal lucro real'
            ],
            [
                'id' => 4,
                'descricao' => 'Regime normal lucro presumido'
            ]
        ]);
    }
}
