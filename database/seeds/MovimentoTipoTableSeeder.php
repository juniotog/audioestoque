<?php

use Illuminate\Database\Seeder;

class MovimentoTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movimento_tipo')->truncate();
        DB::table('movimento_tipo')->insert([
            [
                'id' => 1,
                'descricao' => 'Entradas e/ou aquisições de serviços do estado'
            ],
            [
                'id' => 2,
                'descricao' => 'Entradas e/ou aquisições de serviços de outros estados'
            ],
            [
                'id' => 3,
                'descricao' => 'Entradas e/ou aquisições de serviços do exterior'
            ],
            [
                'id' => 4,
                'descricao' => 'Saídas ou prestações de serviços para o estado'
            ],
            [
                'id' => 5,
                'descricao' => 'Saídas ou prestações de serviços para outros estados'
            ],
            [
                'id' => 6,
                'descricao' => 'Saídas ou prestações de serviços para o exterior'
            ],
        ]);
    }
}



