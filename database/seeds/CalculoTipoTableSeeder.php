<?php

use Illuminate\Database\Seeder;

class CalculoTipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calculo_tipo')->truncate();
        DB::table('calculo_tipo')->insert([
        ]);
    }
}
