<?php

use Illuminate\Database\Seeder;

class PlanosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plano')->truncate();
        DB::table('plano')->insert([
            [
                'id' => 1,
                'limite_cnpj' => 10,
                'limite_pdv' => 10,
                'preco' => 69.99,
                'descricao' => 'Plano básico',
            ],
            [
                'id' => 2,
                'limite_cnpj' => 20,
                'limite_pdv' => 20,
                'preco' => 89.99,
                'descricao' => 'Plano médio',
            ],
            [
                'id' => 3,
                'limite_cnpj' => 40,
                'limite_pdv' => 40,
                'preco' => 139.99,
                'descricao' => 'Plano avançado',
            ],
        ]);
    }
}
