<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado')->truncate();
        DB::table('estado')->insert([
            [
                'id' => '12',
                'sigla' => 'AC',
                'nome' => 'Acre',
                'pais_id' => 1
            ],

            [
                'id' => '27',
                'sigla' => 'AL',
                'nome' => 'Alagoas',
                'pais_id' => 1
            ],

            [
                'id' => '16',
                'sigla' => 'AP',
                'nome' => 'Amapá',
                'pais_id' => 1
            ],

            [
                'id' => '13',
                'sigla' => 'AM',
                'nome' => 'Amazonas',
                'pais_id' => 1
            ],

            [
                'id' => '29',
                'sigla' => 'BA',
                'nome' => 'Bahia',
                'pais_id' => 1
            ],

            [
                'id' => '23',
                'sigla' => 'CE',
                'nome' => 'Ceará',
                'pais_id' => 1
            ],

            [
                'id' => '53',
                'sigla' => 'DF',
                'nome' => 'Distrito Federal',
                'pais_id' => 1
            ],

            [
                'id' => '32',
                'sigla' => 'ES',
                'nome' => 'Espírito Santo',
                'pais_id' => 1
            ],

            [
                'id' => '52',
                'sigla' => 'GO',
                'nome' => 'Goiás',
                'pais_id' => 1
            ],

            [
                'id' => '21',
                'sigla' => 'MA',
                'nome' => 'Maranhão',
                'pais_id' => 1
            ],

            [
                'id' => '51',
                'sigla' => 'MT',
                'nome' => 'Mato Grosso',
                'pais_id' => 1
            ],

            [
                'id' => '50',
                'sigla' => 'MS',
                'nome' => 'Mato Grosso do Sul',
                'pais_id' => 1
            ],

            [
                'id' => '31',
                'sigla' => 'MG',
                'nome' => 'Minas Gerais',
                'pais_id' => 1
            ],

            [
                'id' => '15',
                'sigla' => 'PA',
                'nome' => 'Pará',
                'pais_id' => 1
            ],

            [
                'id' => '25',
                'sigla' => 'PB',
                'nome' => 'Paraíba',
                'pais_id' => 1
            ],

            [
                'id' => '41',
                'sigla' => 'PR',
                'nome' => 'Paraná',
                'pais_id' => 1
            ],

            [
                'id' => '26',
                'sigla' => 'PE',
                'nome' => 'Pernambuco',
                'pais_id' => 1
            ],

            [
                'id' => '22',
                'sigla' => 'PI',
                'nome' => 'Piauí',
                'pais_id' => 1
            ],

            [
                'id' => '33',
                'sigla' => 'RJ',
                'nome' => 'Rio de Janeiro',
                'pais_id' => 1
            ],

            [
                'id' => '24',
                'sigla' => 'RN',
                'nome' => 'Rio Grande do Norte',
                'pais_id' => 1
            ],

            [
                'id' => '43',
                'sigla' => 'RS',
                'nome' => 'Rio Grande do Sul',
                'pais_id' => 1
            ],

            [
                'id' => '11',
                'sigla' => 'RO',
                'nome' => 'Rondônia',
                'pais_id' => 1
            ],

            [
                'id' => '14',
                'sigla' => 'RR',
                'nome' => 'Roraima',
                'pais_id' => 1
            ],

            [
                'id' => '42',
                'sigla' => 'SC',
                'nome' => 'Santa Catarina',
                'pais_id' => 1
            ],

            [
                'id' => '35',
                'sigla' => 'SP',
                'nome' => 'São Paulo',
                'pais_id' => 1
            ],

            [
                'id' => '28',
                'sigla' => 'SE',
                'nome' => 'Sergipe',
                'pais_id' => 1
            ],

            [
                'id' => '17',
                'sigla' => 'TO',
                'nome' => 'Tocantins',
                'pais_id' => 1
            ]
        ]);
    }
}
