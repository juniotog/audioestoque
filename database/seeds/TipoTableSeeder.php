<?php

use Illuminate\Database\Seeder;

class TipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo')->truncate();
        DB::table('tipo')->insert([
            [
                'id' => 1,
                'descricao' => 'Cliente'
            ],
            [
                'id' => 2,
                'descricao' => 'Fornecedor'
            ],
            [
                'id' => 3,
                'descricao' => 'Ambos'
            ]
        ]);
    }
}