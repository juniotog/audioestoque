<?php

namespace SqlEstoque\Validators;

use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class FileExtensionValidator
{
    /**
     * Executa a validação de extensão dos arquivos via upload.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        if ($value instanceof UploadedFile) {
            return in_array($value->getClientOriginalExtension(), $parameters);
        } elseif ($value instanceof File) {
            return in_array($value->getExtension(), $parameters);
        } elseif (is_file($value)) {
            try {
                return in_array((new File($value))->getExtension(), $parameters);
            } catch (\Exception $e) {
                return false;
            }
        }

        return false;
    }

    /**
     * Replacer da mensagem de erro da validação.
     *
     * @param $message
     * @param $attribute
     * @param $rule
     * @param $parameters
     * @return mixed
     */
    public function replacer($message, $attribute, $rule, $parameters)
    {
        return str_replace(':values', implode(',', $parameters), $message);
    }
}