<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    protected $table = 'cliente_fornecedor';

    protected $fillable = ['id', 'tipo_id', 'tipo_pessoa_id', 'cpf', 'cnpj', 'razao_social', 'fantasia', 'nome', 'inscricao_estadual', 'inscricao_municipal', 'rg', 'sexo_id', 'contribuinte_id', 'regime_tributario_id', 'telefone', 'cep', 'rua', 'numero', 'complemento', 'bairro', 'cidade_id', 'email', 'limite', 'status_id', 'observacao', 'observacao_nfe', 'pessoa_tipo_id', 'cliente_id'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }

    public function regime_tributario()
    {
        return $this->belongsTo(RegimeTributario::class);
    }

    public function pessoa_tipo()
    {
        return $this->belongsTo(PessoaTipo::class);
    }

    public function tipo()
    {
        return $this->belongsTo(Tipo::class);
    }

    public function sexo()
    {
        return $this->belongsTo(Sexo::class);
    }

    public function contribuinte()
    {
        return $this->belongsTo(Contribuinte::class);
    }
}
