<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Contribuinte extends Model
{
    protected $table = 'contribuinte';
}
