<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{
    protected $table = 'unidade_medida';
    protected $fillable = ['id','descricao','abreviacao','cliente_id'];
    public $timestamps = false;
}
