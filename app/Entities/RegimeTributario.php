<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class RegimeTributario extends Model
{
    protected $table = 'regime_tributario';
}
