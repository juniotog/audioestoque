<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estado';
}
