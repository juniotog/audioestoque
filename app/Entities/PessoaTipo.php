<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class PessoaTipo extends Model
{
    protected $table = 'pessoa_tipo';
}
