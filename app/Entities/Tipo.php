<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table = 'tipo';

    const CLIENTE = 1;
    const FORNECEDOR = 2;
    const AMBOS = 3;
}
