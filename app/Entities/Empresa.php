<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $fillable = ['id','cnpj','razao_social','fantasia','inscricao_estadual','inscricao_municipal','telefone','rua','numero','bairro','cep','complemento','email','email_contador','logotipo','regime_tributario_id','cnae_principal_id','cnae_secundario_id','ir','csll','pis','confis','inss','iss','serie','num_hom','num_prod','serie_recibo','numero_recibo','certificado_tipo_id','certificado','senha','status_id','cidade_id', 'cliente_id', 'cnae_secundario'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function cnae_principal()
    {
        return $this->belongsTo(Cnae::class, 'cnae_principal_id');
    }

    public function cnae_secundario()
    {
        return $this->belongsTo(Cnae::class, 'cnae_secundario_id');
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }

    public function regime_tributario()
    {
        return $this->belongsTo(RegimeTributario::class);
    }
}
