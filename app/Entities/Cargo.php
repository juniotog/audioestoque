<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
	protected $table = 'cargo';

	protected $fillable = ['descricao', 'cliente_id'];
    /**
     * The roles that belong to the user.
     */
    public function permissoes()
    {
        return $this->belongsToMany(Permissao::class, 'cargo_permissao', 'cargo_id', 'permissao_id');
    }
}
