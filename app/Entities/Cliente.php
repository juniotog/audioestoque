<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    protected $fillable = ['id','cnpj','razao_social','fantasia','inscricao_estadual','inscricao_municipal','telefone','cep','rua','numero','complemento','bairro','cidade_id','email','email_cobranca','status_id','created_users_id','plano_id','created_at','updated_at'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
