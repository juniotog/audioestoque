<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    /**
     * Get the comments for the blog post.
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    /**
     * Get the comments for the blog post.
     */
    public function permissao()
    {
        return $this->hasMany(Permissao::class);
    }
}
