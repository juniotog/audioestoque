<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Rotulo extends Model
{
    protected $table = 'rotulo';
    protected $fillable = ['id', 'descricao', 'cliente_id'];
    public $timestamps = false;
}
