<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    const ATIVO = 1;
    const BLOQUEADO = 2;
    const PENDENTE = 3;
    const INATIVO = 4;
    const EXCLUIR = 5;
}
