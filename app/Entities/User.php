<?php

namespace SqlEstoque\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'usuario_tipo_id', 'plano_id', 'cpf', 'status_id', 'cliente_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cargo()
    {
        return $this->belongsTo(Cargo::class);
    }

    public function plano()
    {
        return $this->belongsTo(Plano::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function funcionario()
    {
        return $this->belongsTo(Status::class, 'users_id');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
}
