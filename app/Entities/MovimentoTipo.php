<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class MovimentoTipo extends Model
{
    protected $table = 'movimento_tipo';
}
