<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class NaturezaOperacao extends Model
{
    protected $table = 'natureza_operacao';
    protected $fillable = ['id', 'descricao', 'movimento_tipo_id', 'cliente_id'];

    public function movimento_tipo()
    {
        return $this->belongsTo(MovimentoTipo::class);
    }
}
