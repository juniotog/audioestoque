<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Permissao extends Model
{
    protected $table = 'permissao';

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
