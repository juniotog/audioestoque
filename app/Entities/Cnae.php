<?php

namespace SqlEstoque\Entities;

use Illuminate\Database\Eloquent\Model;

class Cnae extends Model
{
    protected $table = 'cnae';
    public $incrementing = false;
}
