<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\NaturezaOperacao;
use SqlEstoque\Entities\Status;

class NaturezaOperacaoRepository extends BaseRepository
{
    public function __construct(NaturezaOperacao $rotulo) {
        $this->model = $rotulo;
    }

    public function getbyId($id) {
        return $this->model->where(
            [
                'cliente_id' => $this->cliente_id(),
                'id' => $id
            ]
        )->firstOrFail();
    }

    public function todosNaturezaOp($search) {

        $rotulos = $this->model->with('movimento_tipo')->where('cliente_id', $this->cliente_id());

        if (isset($search)) {
            $rotulos->where('descricao', 'like', '%'.$search.'%');
        }

        return $rotulos->get();
    }

    public function store($data) {
        $data['cliente_id'] = $this->cliente_id();
        return $this->model->create($data);
    }
}