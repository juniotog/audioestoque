<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Cliente;
use SqlEstoque\Entities\Status;
use SqlEstoque\Entities\Tipo;

use Illuminate\Support\Facades\Auth;

class ClienteRepository extends BaseRepository
{
    public function __construct(Cliente $cliente) {
        $this->model = $cliente;
    }

    //with(['status', 'tipo', 'sexo', 'contribuinte', 'pessoa_tipo', 'regime_tributario'])

    public function getbyId($id) {
        return $this->model->with(['status','cidade.estado'])->where(['id' => $id])->first();
    }

    public function todosClientes($search) {
        return $this->buscarClientes($search);
    }

    public function buscarClientes($data) {
        $search = $data['search'];
        $clientes = $this->model->with(['status', 'users']);

        if (isset($search)) {
            $clientes->orWhere('cnpj', 'like', '%'.$search.'%');
            $clientes->orWhere('razao_social', 'like', '%'.$search.'%');
            $clientes->orWhere('email', 'like', '%'.$search.'%');
        }

        $clientes->whereIn('status_id', [
            Status::ATIVO,
            Status::BLOQUEADO,
            Status::PENDENTE
        ]);

        return $clientes->paginate(15);
    }

    public function store($data) {
        $data['created_users_id'] = Auth::user()->id;
        return $this->model->create($data);
    }
}