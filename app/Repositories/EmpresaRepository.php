<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Empresa;
use SqlEstoque\Entities\Status;

class EmpresaRepository extends BaseRepository
{
    public function __construct(Empresa $empresa) {
        $this->model = $empresa;
    }


    public function todasEmpresasUser() {

        $empresas = $this->model->where('cliente_id', $this->cliente_id());

        return $empresas->get();
    }

    public function getbyId($id) {
        return $this->model->with(['status', 'regime_tributario', 'cidade.estado', 'cnae_principal', 'cnae_secundario'])->findOrFail($id);
    }

    public function todasEmpresas($search) {
        $empresas = $this->model->with('status');

        if (isset($search)) {
            $empresas->where('razao_social', 'like', '%'.$search.'%');
            $empresas->orWhere('inscricao_estadual', 'like', '%'.$search.'%');
            $empresas->orWhere('cnpj', 'like', '%'.$search.'%');
        }

        $empresas->where('cliente_id', $this->cliente_id());

        $empresas->whereIn('status_id', [
            Status::ATIVO,
            Status::BLOQUEADO,
            Status::PENDENTE
        ]);

        return $empresas->paginate(15);
    }

    public function store($data) {
        $data['cliente_id'] = $this->cliente_id();
        return $this->model->create($data);
    }
}