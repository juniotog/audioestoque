<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Medida;
use SqlEstoque\Entities\Status;

class MedidaRepository extends BaseRepository
{
    public function __construct(Medida $medida) {
        $this->model = $medida;
    }

    public function getbyId($id) {
        return $this->model->findOrFail($id);
    }

    public function todasMedidas($search) {

        $medidas = $this->model->where('cliente_id', $this->cliente_id());

        if (isset($search)) {
            $medidas->where('descricao', 'like', '%'.$search.'%')
            ->orWhere('abreviacao', 'like', '%'.$search.'%');
        }

        return $medidas->get();
    }

    public function store($data) {
        $data['cliente_id'] = $this->cliente_id();
        return $this->model->create($data);
    }
}