<?php

namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Status;

use Illuminate\Support\Facades\Auth;

abstract class BaseRepository
{
    /**
     * The Model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Destroy a model.
     *
     * @param  int $id
     * @return void
     */


    public function destroy($id)
    {
        $this->getById($id)->delete();
    }
    /**
     * Get Model by id.
     *
     * @param  int  $id
     * @return \App\Models\Model
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function find($id) {
        return $this->model->find($id);
    }

    /**
     * Get all data model
     * @return Array Model
     */
    public function getAll() {
        return $this->model->whereIn('status_id', [
                Status::ATIVO,
                Status::BLOQUEADO,
                Status::PENDENTE
            ])->all();
    }

    public function update($data, $id) {
        $model = $this->model->find($id)->fill($data);

        return ( $model->update() ) ? $model : false;
    }

    public function cliente_id() {
        return Auth::user()->cliente_id;
    }
}