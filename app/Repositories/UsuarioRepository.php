<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\User;
use SqlEstoque\Entities\Status;

class UsuarioRepository extends BaseRepository
{
    public function __construct(User $user) {
        $this->model = $user;
    }

    public function getbyId($id) {
        return $this->model->with('cliente')->findOrFail($id);
    }

    public function todosFuncionarios($search) {
        return $this->searchUsuario($search, 3);
    }

    public function todosClientesUsuarios($search) {
        return $this->searchUsuario($search, 2);
    }

    public function searchUsuario($search, $status_id) {
        $usuarios = $this->model->with(['status', 'cliente'])->where('usuario_tipo_id', $status_id);

        if (isset($search)) {
            $usuarios->where('name', 'like', '%'.$search.'%');
            $usuarios->orWhere('cpf', 'like', '%'.$search.'%');
            $usuarios->orWhere('email', 'like', '%'.$search.'%');
        }

        $usuarios->whereIn('status_id', [
            Status::ATIVO,
            Status::BLOQUEADO,
            Status::PENDENTE
        ]);

        if ($status_id == 3) {
            $usuarios->where('cliente_id', $this->cliente_id());
        }

        return $usuarios->paginate(15);
    }

    public function store($data) {
        return $this->model->create($data);
    }
}