<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Fornecedor;
use SqlEstoque\Entities\Status;
use SqlEstoque\Entities\Tipo;

class FornecedorRepository extends BaseRepository
{
    public function __construct(Fornecedor $cliente) {
        $this->model = $cliente;
    }

    //with(['status', 'tipo', 'sexo', 'contribuinte', 'pessoa_tipo', 'regime_tributario'])

    public function getbyId($id) {
        return $this->model->with(['status', 'tipo', 'sexo', 'contribuinte', 'pessoa_tipo', 'regime_tributario'])->where(['cliente_id' => $this->cliente_id(), 'id' => $id])->first();
    }

    public function todosClientes($search) {
        return $this->buscarClientes($search);
    }

    public function buscarClientes($data) {

        $search = $data['search'];
        $clientes = $this->model->with(['status', 'pessoa_tipo']);

        if (isset($search)) {
            $clientes->where('nome', 'like', '%'.$search.'%');
            $clientes->orWhere('cpf', 'like', '%'.$search.'%');
            $clientes->orWhere('cnpj', 'like', '%'.$search.'%');
            $clientes->orWhere('razao_social', 'like', '%'.$search.'%');
            $clientes->orWhere('email', 'like', '%'.$search.'%');
        }

        if (isset($data['tipo_id']) && $data['tipo_id'] != Tipo::AMBOS) {
            $clientes->whereIn('tipo_id', [$data['tipo_id'], Tipo::AMBOS]);
        }

        $clientes->whereIn('status_id', [
            Status::ATIVO,
            Status::BLOQUEADO,
            Status::PENDENTE
        ]);

        $clientes->where('cliente_id', $this->cliente_id());

        return $clientes->paginate(15);
    }

    public function store($data) {
        $data['cliente_id'] = $this->cliente_id();
        return $this->model->create($data);
    }
}