<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Cargo;
use SqlEstoque\Entities\Status;
use SqlEstoque\Entities\Menu;

class CargoRepository extends BaseRepository
{
    public function __construct(Cargo $cliente) {
        $this->model = $cliente;
    }

    //with(['status', 'tipo', 'sexo', 'contribuinte', 'pessoa_tipo', 'regime_tributario'])

    public function getbyId($id) {
        //return $this->model->with(['status', 'tipo', 'sexo', 'contribuinte', 'pessoa_tipo', 'regime_tributario'])->where(['cliente_id' => $this->cliente_id(), 'id' => $id])->first();
    }

    public function todosCargos($search) {
        return $this->buscarCargos($search);
    }

    public function getPermissoes() {
        return Menu::with('permissao')->orderBy('descricao')->get();
    }

    public function buscarCargos($data) {

        $search = $data['search'];
        $cargos = $this->model->with(['permissoes']);

        if (isset($search)) {
            $cargos->where('descricao', 'like', '%'.$search.'%');
        }

        $cargos->where('cliente_id', $this->cliente_id());

        return $cargos->paginate(15);
    }

    public function store($data) {
        $data['cliente_id'] = $this->cliente_id();
        $cargo = $this->model->create($data);
        $cargo->permissoes()->sync($data['permissoes']);
        return $cargo;
    }
}