<?php
namespace SqlEstoque\Repositories;

use SqlEstoque\Entities\Rotulo;
use SqlEstoque\Entities\Status;

class RotuloRepository extends BaseRepository
{
    public function __construct(Rotulo $rotulo) {
        $this->model = $rotulo;
    }

    public function getbyId($id) {
        return $this->model->findOrFail($id);
    }

    public function todosRotulos($search) {

        $rotulos = $this->model->where('cliente_id', $this->cliente_id());

        if (isset($search)) {
            $rotulos->where('descricao', 'like', '%'.$search.'%')
            ->orWhere('abreviacao', 'like', '%'.$search.'%');
        }

        return $rotulos->get();
    }

    public function store($data) {
        $data['cliente_id'] = $this->cliente_id();
        return $this->model->create($data);
    }
}