<?php

namespace SqlEstoque\Http\Requests\Empresa;

use SqlEstoque\Http\Requests\BaseRequest;

class UpdateEmpresa extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $empresa_id = $this->route('empresa');
        $cliente_id = $this->user()->cliente_id;;

        return [
            'cnpj' => 'required|size:18|unique:empresa,cnpj,'.$empresa_id.',id,cliente_id,'.$cliente_id,
            'razao_social' => 'required',
            'fantasia' => 'required',
            'inscricao_estadual' => 'required',
            'inscricao_municipal' => 'nullable',
            'telefone' => 'required|min:14|max:15',
            'email' => 'required|email|max:120',
            'email_contador' => 'required|email|max:120',
            'cep' => 'required|size:10',
            'logotipo' => 'nullable',
            'rua' => 'required',
            'numero' => 'required',
            'complemento' => '',
            'bairro' => 'required',
            'cidade_id' => 'required',
            'regime_tributario_id' => 'required',
            'cnae_principal_id' => 'required',
            'cnae_secundario_id' => 'required',
            'serie' => 'required',
            'num_hom' => 'required',
            'num_prod' => 'required',
            'certificado_tipo_id' => 'required',
            'certificado' => 'nullable|max:64|extension:pfx,p12,cer,p7b'
        ];
    }

    public function messages() {
        return [
            'certificado.extension' => 'Extensão de arquivo não permitida.'
        ];
    }
}
