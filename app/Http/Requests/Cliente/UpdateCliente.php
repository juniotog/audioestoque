<?php

namespace SqlEstoque\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCliente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route("cliente");

        return [
            'cnpj' => 'nullable|required_if:pessoa_tipo_id,2|size:18|unique:cliente_fornecedor,cnpj,'.$id.',id',
            'razao_social' => 'required_if:pessoa_tipo_id,2|max:120',
            'fantasia' => 'required_if:pessoa_tipo_id,2|max:120',
            'inscricao_estadual' => 'required_if:pessoa_tipo_id,2|max:20',
            'inscricao_municipal' => '|max:20',
            'telefone' => 'required|max:15',
            'email' => 'required|max:140',
            'email_cobranca' => 'required|max:140',
            'cep' => 'required|size:10',
            'rua' => 'required|max:100',
            'numero' => 'required|max:5',
            'complemento' => 'max:40',
            'bairro' => 'required|max:60',
            'cidade_id' => 'required',
            'plano_id' => 'required'
        ];
    }
}
