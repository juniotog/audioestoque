<?php

namespace SqlEstoque\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Auth;

abstract class BaseRequest extends FormRequest
{
    protected $cliente_id;

    public function __construct() {
        $this->cliente_id = Auth::user()->cliente_id;
    }
}