<?php

namespace SqlEstoque\Http\Requests\Rotulo;

use SqlEstoque\Http\Requests\BaseRequest;

class UpdateRotulo extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $medida_id = $this->route('rotulo');
        return [
            'descricao' => 'required|max:45|unique:rotulo,descricao,'.$medida_id.',id,cliente_id,'.$this->cliente_id,
        ];
    }
}
