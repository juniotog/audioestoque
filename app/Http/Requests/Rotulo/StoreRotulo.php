<?php

namespace SqlEstoque\Http\Requests\Rotulo;

use SqlEstoque\Http\Requests\BaseRequest;

class StoreRotulo extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required|max:45|unique:rotulo,descricao,null,id,cliente_id,'.$this->cliente_id,
        ];
    }
}
