<?php

namespace SqlEstoque\Http\Requests\Medida;

use SqlEstoque\Http\Requests\BaseRequest;

class StoreMedida extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required|max:45',
            'abreviacao' => 'required|max:3|unique:unidade_medida,abreviacao,null,id,cliente_id,'.$this->cliente_id,
        ];
    }
}
