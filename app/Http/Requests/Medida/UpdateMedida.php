<?php

namespace SqlEstoque\Http\Requests\Medida;

use SqlEstoque\Http\Requests\BaseRequest;

class UpdateMedida extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $medida_id = $this->route('medida');
        return [
            'descricao' => 'required|max:45',
            'abreviacao' => 'required|max:3|unique:unidade_medida,abreviacao,'.$medida_id.',id,cliente_id,'.$this->cliente_id,
        ];
    }
}
