<?php

namespace SqlEstoque\Http\Requests\NaturezaOperacao;

use SqlEstoque\Http\Requests\BaseRequest;
use Illuminate\Http\Request;

class StoreNaturezaOperacao extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $movimento_tipo_id = $request->get('movimento_tipo_id');

        return [
            'descricao' => 'required|max:45|unique:natureza_operacao,descricao,null,id,cliente_id,'.$this->cliente_id. ',movimento_tipo_id,'.$movimento_tipo_id,
            'movimento_tipo_id' => 'required'
        ];
    }
}
