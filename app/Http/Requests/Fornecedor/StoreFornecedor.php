<?php

namespace SqlEstoque\Http\Requests\Fornecedor;

use SqlEstoque\Http\Requests\BaseRequest;

class StoreFornecedor extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'pessoa_tipo_id' => 'required',
            'tipo_id' => 'required',
            'cnpj' => 'nullable|required_if:pessoa_tipo_id,2|size:18|unique:cliente_fornecedor,cnpj,null,id,cliente_id,'.$this->cliente_id,
            'razao_social' => 'required_if:pessoa_tipo_id,2|max:120',
            'fantasia' => 'required_if:pessoa_tipo_id,2|max:120',
            'cpf' => 'nullable|required_if:pessoa_tipo_id,1|size:14|unique:cliente_fornecedor,cpf,null,id,cliente_id,'.$this->cliente_id,
            'rg' => 'required_if:pessoa_tipo_id,1|max:15',
            'nome' => 'required_if:pessoa_tipo_id,1|max:120',
            'inscricao_estadual' => 'required_if:pessoa_tipo_id,2|max:20',
            'inscricao_municipal' => '|max:20',
            'sexo_id' => 'required_if:pessoa_tipo_id,1',
            'contribuinte_id' => 'required',
            'regime_tributario_id' => 'required_if:pessoa_tipo_id,2',
            'telefone' => 'required|max:15',
            'email' => 'required|max:140',
            'limite' => 'nullable',
            'cep' => 'required|size:10',
            'rua' => 'required|max:100',
            'numero' => 'required|max:5',
            'complemento' => 'max:40',
            'bairro' => 'required|max:60',
            'cidade_id' => 'required',
        ];
    }
}
