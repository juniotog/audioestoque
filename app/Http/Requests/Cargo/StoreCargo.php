<?php

namespace SqlEstoque\Http\Requests\Cargo;

use Illuminate\Foundation\Http\FormRequest;

class StoreCargo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cliente_id = $this->user()->cliente_id;

        return [
            'descricao' => 'required|min:4|unique:cargo,descricao,null,id,cliente_id,'.$cliente_id,
            'permissoes.0' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'permissoes.0.required' => 'Selecione ao menos uma permissão ao cargo.'
        ];
    }
}
