<?php

namespace SqlEstoque\Http\Requests\Usuario;

use SqlEstoque\Http\Requests\BaseRequest;

class StoreUsuario extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6',
            'email' => 'required|unique:users|email',
            'password' => 'required|confirmed|min:4',
            'cpf' => 'required|unique:users|size:14',
            'cliente_id' => 'required',
        ];


    }
}
