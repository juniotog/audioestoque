<?php

namespace SqlEstoque\Http\Requests\Usuario;

use SqlEstoque\Http\Requests\BaseRequest;

class UpdateUsuario extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('usuario');

        return [
            'name' => 'required|min:6',
            'email' => 'required|unique:users,email,'.$id.'|email',
            'password' => 'nullable|confirmed|min:4',
            'cpf' => 'required|unique:users,cpf,'.$id.'|size:14',
            'cliente_id' => 'required',
            'status_id' => 'in:1,2,3'
        ];
    }
}
