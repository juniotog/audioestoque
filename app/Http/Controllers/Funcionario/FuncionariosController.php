<?php

namespace SqlEstoque\Http\Controllers\Funcionario;


use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

// Repository
use SqlEstoque\Repositories\UsuarioRepository;
use SqlEstoque\Repositories\EmpresaRepository;

// Requests
use SqlEstoque\Http\Requests\Funcionario\StoreFuncionario;
use SqlEstoque\Http\Requests\Funcionario\UpdateFuncionario;

use SqlEstoque\Entities\Status;

class FuncionariosController extends Controller
{
    /**
     * Repositorio
     * @var UsuarioRepository
     */
    private $repository;

    private $empresaRepository;

    /**
     * Contrução da classe
     * @param UsuarioRepository $repository
     */
    public function __construct(UsuarioRepository $repository, EmpresaRepository $empresaRepository) {
        $this->repository = $repository;
        $this->empresaRepository = $empresaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = $this->repository->todosFuncionarios($request['search']);
        $users->appends($request->toArray());
        return view('funcionarios.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('funcionarios.create', [
            'planos' => DB::table('plano')->get(),
            'empresas' => $this->empresaRepository->todasEmpresasUser()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFuncionario $request)
    {
        $data = $request->all();
        $data['password'] =  \Hash::make($request['password']);
        $data['cliente_id'] = $this->repository->cliente_id();

        $this->repository->store($data);

        return \Redirect::route('funcionarios.index')
        ->with('success', [
            'title' => 'Funcionário cadastrado',
            'message' => 'Funcionário cadastrado com sucesso'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->repository->getById($id);
        return view('funcionarios.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->repository->getById($id);
        $planos = DB::table('plano')->get();
        $empresas = DB::table('empresa')->get();
        $status = DB::table('status')->whereIn('id', [
            Status::ATIVO,
            Status::BLOQUEADO,
            Status::PENDENTE
        ])->get();

        return view('funcionarios.edit', compact('user', 'planos', 'status', 'empresas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFuncionario $request, $id)
    {
        $request = $request->all();

        if (isset($request['password']))
            $request['password'] =  \Hash::make($request['password']);
        else
            unset($request['password']);

        $this->repository->update($request, $id);

        return \Redirect::route('funcionarios.index')
        ->with('success', [
            'title' => 'Funcionário editado',
            'message' => 'Funcionário editado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = $this->repository->find($id);

        $user->status_id = 5;

        $user->save();

        return \Redirect::route('funcionarios.index')
        ->with('success', [
            'title' => 'Funcionário excluído',
            'message' => 'Funcionário excluído com sucesso'
        ]);
    }
}
