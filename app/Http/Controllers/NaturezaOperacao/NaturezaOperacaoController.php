<?php

namespace SqlEstoque\Http\Controllers\NaturezaOperacao;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;

use SqlEstoque\Entities\MovimentoTipo;

// Repository
use SqlEstoque\Repositories\NaturezaOperacaoRepository;

// Requests
use SqlEstoque\Http\Requests\NaturezaOperacao\StoreNaturezaOperacao;
use SqlEstoque\Http\Requests\NaturezaOperacao\UpdateNaturezaOperacao;

class NaturezaOperacaoController extends Controller
{
    /**
     * Repositorio
     * @var NaturezaOperacaoRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param NaturezaOperacaoRepository $repository
     */
    public function __construct(NaturezaOperacaoRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $naturezaOperacao = $this->repository->todosNaturezaOp($request['search']);
        return view('natureza-operacao.index', compact('naturezaOperacao'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $movimento_tipos = MovimentoTipo::orderBy('descricao')->get();
        return view('natureza-operacao.create', compact('movimento_tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNaturezaOperacao $request)
    {
        $request = $request->all();

        $this->repository->store($request);

        return \Redirect::route('natureza-operacao.index')
        ->with('success', [
            'title' => 'Natureza da operação cadastrada',
            'message' => 'Natureza da operação cadastrada com sucesso'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $naturezaOperacao = $this->repository->getById($id);
        $movimento_tipos = MovimentoTipo::orderBy('descricao')->get();
        return view('natureza-operacao.edit', compact('movimento_tipos', 'naturezaOperacao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNaturezaOperacao $request, $id)
    {
        $request = $request->all();
        $this->repository->update($request, $id);

        return \Redirect::route('natureza-operacao.index')
        ->with('success', [
            'title' => 'Natureza da operação editada',
            'message' => 'Natureza da operação editada com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->destroy($id);

        return \Redirect::route('natureza-operacao.index')
        ->with('success', [
            'title' => 'Natureza da operação excluída',
            'message' => 'Natureza da operação excluída com sucesso'
        ]);
    }
}
