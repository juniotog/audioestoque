<?php

namespace SqlEstoque\Http\Controllers\Empresa;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

// Repository
use SqlEstoque\Repositories\EmpresaRepository;

// Request
use SqlEstoque\Http\Requests\Empresa\StoreEmpresa;
use SqlEstoque\Http\Requests\Empresa\UpdateEmpresa;

class EmpresasController extends Controller
{
    /**
     * Repositorio
     * @var EmpresaRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param EmpresaRepository $repository
     */
    public function __construct(EmpresaRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $empresas = $this->repository->todasEmpresas($request['search']);
        $empresas->appends($request->toArray());

        return view('empresas.index', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresas.create', [
            'cnaes' => DB::table('cnae')->orderBy('descricao', 'asc')->get(),
            'regime_tributario' => DB::table('regime_tributario')->orderBy('descricao', 'asc')->get(),
            'estados' => DB::table('estado')->orderBy('nome', 'asc')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmpresa $request)
    {
        $data = $request->all();

        if ($request->hasFile('certificado')) {
            $certificate = $request->file('certificado');
            $data['certificado'] = base64_encode(file_get_contents($certificate->getRealPath()));
        }

        if ($request->hasFile('logotipo')) {
            $certificate = $request->file('logotipo');
            $data['logotipo'] = base64_encode(file_get_contents($certificate->getRealPath()));
        }

        if (isset($request->senha)) {
            $data['senha'] = encrypt($data['senha']);
        }

        $this->repository->store($data);

        return \Redirect::route('empresas.index')
        ->with('success', [
            'title' => 'Empresa cadastrada',
            'message' => 'Empresa cadastrada com sucesso'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = $this->repository->getById($id);
        return view('empresas.show', compact('empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = $this->repository->getById($id);
        $cnaes = DB::table('cnae')->orderBy('descricao', 'asc')->get();
        $regime_tributario = DB::table('regime_tributario')->orderBy('descricao', 'asc')->get();
        $estados = DB::table('estado')->orderBy('nome', 'asc')->get();

        return view('empresas.edit', compact('empresa','cnaes','regime_tributario','estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmpresa $request, $id)
    {
        $data = $request->all();
        if (isset($request->senha)) {
            $data['senha'] = encrypt($data['senha']);
        }

        if ($request->hasFile('certificado')) {
            $certificate = $request->file('certificado');
            $data['certificado'] = base64_encode(file_get_contents($certificate->getRealPath()));
        }

        if ($request->hasFile('logotipo')) {
            $certificate = $request->file('logotipo');
            $data['logotipo'] = base64_encode(file_get_contents($certificate->getRealPath()));
        }

        $this->repository->update($data, $id);

        return \Redirect::route('empresas.index')
        ->with('success', [
            'title' => 'Empresa editada',
            'message' => 'Empresa editada com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = $this->repository->find($id);

        $empresa->status_id = 5;

        $empresa->save();

        return \Redirect::route('empresas.index')
        ->with('success', [
            'title' => 'Empresa excluída',
            'message' => 'Empresa excluída com sucesso'
        ]);
    }
}
