<?php

namespace SqlEstoque\Http\Controllers\Cidade;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SqlEstoque\Http\Controllers\Controller;

class CidadesController extends Controller
{
    public function getCidades($estadoId) {
        return response()->json(
            DB::table('cidade')->where('estado_id', $estadoId)->orderBy('nome')->get()
        );
    }
}
