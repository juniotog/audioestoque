<?php

namespace SqlEstoque\Http\Controllers\Usuario;


use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

// Repository
use SqlEstoque\Repositories\UsuarioRepository;

// Requests
use SqlEstoque\Http\Requests\Usuario\StoreUsuario;
use SqlEstoque\Http\Requests\Usuario\UpdateUsuario;

use SqlEstoque\Entities\Status;
use SqlEstoque\Entities\Cliente;

class UsuariosController extends Controller
{
    /**
     * Repositorio
     * @var UsuarioRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param UsuarioRepository $repository
     */
    public function __construct(UsuarioRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = $this->repository->todosClientesUsuarios($request['search']);
        $users->appends($request->toArray());
        return view('usuarios.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create', [
            'clientes' => Cliente::select(['id', 'razao_social', 'cnpj'])->orderBy('razao_social')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsuario $request)
    {
        $request = $request->all();
        $request['password'] =  \Hash::make($request['password']);

        $this->repository->store($request);

        return \Redirect::route('usuarios.index')
        ->with('success', [
            'title' => 'Usuário cadastrado',
            'message' => 'Usuário cadastrado com sucesso'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->repository->getById($id);
        return view('usuarios.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->repository->getById($id);
        $status = DB::table('status')->whereIn('id', [
            Status::ATIVO,
            Status::BLOQUEADO,
            Status::PENDENTE
        ])->get();
        $clientes = Cliente::select(['id', 'razao_social', 'cnpj'])->orderBy('razao_social')->get();

        return view('usuarios.edit', compact('user', 'planos', 'status', 'clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsuario $request, $id)
    {
        $request = $request->all();

        if (isset($request['password']))
            $request['password'] =  \Hash::make($request['password']);
        else
            unset($request['password']);

        $this->repository->update($request, $id);

        return \Redirect::route('usuarios.index')
        ->with('success', [
            'title' => 'Usuário editado',
            'message' => 'Usuário editado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = $this->repository->find($id);

        $user->status_id = 5;

        $user->save();

        return \Redirect::route('usuarios.index')
        ->with('success', [
            'title' => 'Usuário excluído',
            'message' => 'Usuário excluído com sucesso'
        ]);
    }
}
