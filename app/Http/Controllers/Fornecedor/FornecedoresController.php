<?php

namespace SqlEstoque\Http\Controllers\Fornecedor;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;

// Entities
use SqlEstoque\Entities\PessoaTipo;
use SqlEstoque\Entities\Sexo;
use SqlEstoque\Entities\Contribuinte;
use SqlEstoque\Entities\RegimeTributario;
use SqlEstoque\Entities\Estado;
use SqlEstoque\Entities\Tipo;

// Repository
use SqlEstoque\Repositories\FornecedorRepository;

// Request
use SqlEstoque\Http\Requests\Fornecedor\StoreFornecedor;
use SqlEstoque\Http\Requests\Fornecedor\UpdateFornecedor;

class FornecedoresController extends Controller
{
    /**
     * Repositorio
     * @var FornecedorRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param FornecedorRepository $repository
     */
    public function __construct(FornecedorRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clientes = $this->repository->todosClientes($request);
        $clientes->appends($request->toArray());

        $tipos = Tipo::orderBy('descricao')->get();

        return view('fornecedores.index', compact('clientes', 'tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'pessoa_tipo' => PessoaTipo::orderBy('descricao')->get(),
            'sexo' => Sexo::orderBy('descricao')->get(),
            'contribuinte' => Contribuinte::orderBy('descricao')->get(),
            'regime_tributario' => RegimeTributario::orderBy('descricao')->get(),
            'estados' => Estado::orderBy('nome', 'asc')->get(),
            'tipos' => Tipo::orderBy('descricao')->get()
        ];

        return view('fornecedores.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFornecedor $request)
    {
        $data = $request->all();

        $this->repository->store($data);

        return \Redirect::route('fornecedores.index')
        ->with('success', [
            'title' => 'Cliente/fornecedor cadastrado',
            'message' => 'Cliente/fornecedor cadastrado com sucesso'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = $this->repository->getById($id);
        return view('fornecedores.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'cliente' => $this->repository->getbyId($id),
            'pessoa_tipo' => PessoaTipo::orderBy('descricao')->get(),
            'sexo' => Sexo::orderBy('descricao')->get(),
            'contribuinte' => Contribuinte::orderBy('descricao')->get(),
            'regime_tributario' => RegimeTributario::orderBy('descricao')->get(),
            'estados' => Estado::orderBy('nome', 'asc')->get(),
            'tipos' => Tipo::orderBy('descricao')->get()
        ];

        return view('fornecedores.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFornecedor $request, $id)
    {
        $data = $request->all();

        $this->repository->update($data, $id);

        return \Redirect::route('fornecedores.index')
        ->with('success', [
            'title' => 'Cliente/fornecedor editado',
            'message' => 'Cliente/fornecedor editado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = $this->repository->find($id);

        $cliente->status_id = 5;

        $cliente->save();

        return \Redirect::route('fornecedores.index')
        ->with('success', [
            'title' => 'Cliente/fornecedor excluído',
            'message' => 'Cliente/fornecedor excluído com sucesso'
        ]);
    }
}
