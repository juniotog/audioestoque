<?php

namespace SqlEstoque\Http\Controllers\Cargo;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;

// Repository
use SqlEstoque\Repositories\CargoRepository;

// Request
use SqlEstoque\Http\Requests\Cargo\StoreCargo;
use SqlEstoque\Http\Requests\Cargo\UpdateCargo;

class CargosController extends Controller
{
    /**
     * Repositorio
     * @var CargoRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param EmpresaRepository $repository
     */
    public function __construct(CargoRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cargos = $this->repository->todosCargos($request);
        return view('cargos.index', compact('cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menuPermissoes = $this->repository->getPermissoes();
        return view('cargos.create', compact('menuPermissoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCargo $request)
    {
        $data = $request->all();
        $this->repository->store($data);

        return \Redirect::route('cargos.index')
        ->with('success', [
            'title' => 'Cargo cadastrado',
            'message' => 'Cargo cadastrado com sucesso'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCargo $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
