<?php

namespace SqlEstoque\Http\Controllers\Auth;

use SqlEstoque\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status_id' => 1];
    }

    public function buscaMenu($menu) {
        if (isset($menu->menu_id) && !is_null($menu->menu_id)) {
           $menu['anterior'] = $this->buscaMenu($menu->menu);
        }

        return $menu->toArray();
    }

    public function montaMenu(&$menus, $menu) {

        if (is_null($menu['menu_id'])) {
            $exist = false;
            foreach ($menus as $fmenu) {
                if ($fmenu['id'] == $menu['id']) {
                    $exist = true;
                }
            }

            if (!$exist) {
                $i = count($menus);
                $menus[$i] = $menu;
            }

            return;
        } else {

            foreach ($menus as $key => $fmenu) {
                if ($fmenu['id'] == $menu['menu_id']) {
                    $exist = false;

                    if (isset($fmenu['proximo'])) 
                        foreach ($fmenu['proximo'] as $amenu) 
                            if ($amenu['id'] == $menu['id']) 
                                $exist = true;

                    if (!$exist) {

                        if (!isset($menus[$key]['proximo'])) 
                            $menus[$key]['proximo'] = [];

                        $i = count($menus[$key]['proximo']);

                        $insereMenu = $menu;

                        if (isset($insereMenu['menu'])) unset($insereMenu['menu']);
                        if (isset($insereMenu['anterior'])) unset($insereMenu['anterior']);

                        $menus[$key]['proximo'][$i] = $insereMenu;
                    }
                }
            }

            return $this->montaMenu($menus, $menu['anterior']);
        }

        return $menu;
    }

    public function ordernarMenu(&$menus) {
        usort($menus, array($this, 'ordenarMenuOrdem'));

        foreach($menus as $key => $menu) {
            usort($menu['proximo'], array($this, 'ordenarMenuOrdem'));
            $menus[$key]['proximo'] = $menu['proximo'];
        }
    }

    public function authenticated(Request $request, $user) {
        $menus = [];

        // Se for administrador do sistema tem acesso full
        if ($user->usuario_tipo_id == 1)
            $permissoes =  \SqlEstoque\Entities\Permissao::all();
        else
            $permissoes = $user->cargo->permissoes;
    
        foreach($permissoes as $permissao) {
            $this->montaMenu($menus, $this->buscaMenu($permissao->menu));
        }

        $this->ordernarMenu($menus);
        $request->session()->put('menus', $menus);
    }

    public function ordenarMenuOrdem($a, $b) {
        if($a['descricao'] == $b['descricao']) {
            return 0;
        }

         return ($a['descricao'] > $b['descricao']) ? +1 : -1;
    }
}
