<?php

namespace SqlEstoque\Http\Controllers\Rotulo;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;

// Repository
use SqlEstoque\Repositories\RotuloRepository;

// Requests
use SqlEstoque\Http\Requests\Rotulo\StoreRotulo;
use SqlEstoque\Http\Requests\Rotulo\UpdateRotulo;


class RotulosController extends Controller
{
    /**
     * Repositorio
     * @var RotuloRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param RotuloRepository $repository
     */
    public function __construct(RotuloRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rotulos = $this->repository->todosRotulos($request['search']);
        return view('rotulos.index', compact('rotulos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rotulos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRotulo $request)
    {
        $request = $request->all();

        $this->repository->store($request);

        return \Redirect::route('rotulos.index')
        ->with('success', [
            'title' => 'Rótulo cadastrado',
            'message' => 'Rótulo cadastrado com sucesso'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rotulo = $this->repository->getById($id);
        return view('rotulos.edit', compact('rotulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRotulo $request, $id)
    {
        $request = $request->all();
        $this->repository->update($request, $id);

        return \Redirect::route('rotulos.index')
        ->with('success', [
            'title' => 'Rótulo editado',
            'message' => 'Rótulo editado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->destroy($id);

        return \Redirect::route('rotulos.index')
        ->with('success', [
            'title' => 'Rótulo excluído',
            'message' => 'Rótulo excluído com sucesso'
        ]);
    }
}
