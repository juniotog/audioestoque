<?php

namespace SqlEstoque\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;

// Entities
use SqlEstoque\Entities\Status;
use SqlEstoque\Entities\Estado;
use SqlEstoque\Entities\User;
use SqlEstoque\Entities\Plano;

// Repository
use SqlEstoque\Repositories\ClienteRepository;

// Request
use SqlEstoque\Http\Requests\Cliente\StoreCliente;
use SqlEstoque\Http\Requests\Cliente\UpdateCliente;

class ClientesController extends Controller
{
    /**
     * Repositorio
     * @var ClienteRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param ClienteRepository $repository
     */
    public function __construct(ClienteRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clientes = $this->repository->todosClientes($request);
        $clientes->appends($request->toArray());

        return view('clientes.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'estados' => Estado::orderBy('nome', 'asc')->get(),
            'planos' => Plano::orderBy('id')->get()
        ];

        return view('clientes.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCliente $request)
    {
        $data = $request->all();

        $this->repository->store($data);

        return \Redirect::route('clientes.index')
        ->with('success', [
            'title' => 'Cliente cadastrado',
            'message' => 'Cliente cadastrado com sucesso'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = $this->repository->getById($id);

        return view('clientes.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'cliente' => $this->repository->getbyId($id),
            'planos' => Plano::orderBy('id')->get(),
            'estados' => Estado::orderBy('nome', 'asc')->get(),
            'status' => Status::whereIn('id', [
                Status::ATIVO,
                Status::BLOQUEADO,
                Status::PENDENTE
            ])->get()
        ];

        return view('clientes.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCliente $request, $id)
    {
        $data = $request->all();

        if (in_array((int)$data['status_id'], [2, 3]))
            User::where(['cliente_id' => $id, 'status_id' => 1])->update(['status_id' => 4]);
        else if((int)$data['status_id'] == 1)
            User::where(['cliente_id' => $id, 'status_id' => 4])->update(['status_id' => 1]);

        $this->repository->update($data, $id);

        return \Redirect::route('clientes.index')
        ->with('success', [
            'title' => 'Cliente editado',
            'message' => 'Cliente editado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = $this->repository->find($id);

        $cliente->status_id = 5;
        $cliente->users()->update(['status_id' => 4]);

        $cliente->save();

        return \Redirect::route('clientes.index')
        ->with('success', [
            'title' => 'Cliente excluído',
            'message' => 'Cliente excluído com sucesso'
        ]);
    }
}
