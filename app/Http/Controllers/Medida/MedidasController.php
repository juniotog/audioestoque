<?php

namespace SqlEstoque\Http\Controllers\Medida;

use Illuminate\Http\Request;
use SqlEstoque\Http\Controllers\Controller;

// Repository
use SqlEstoque\Repositories\MedidaRepository;

// Requests
use SqlEstoque\Http\Requests\Medida\StoreMedida;
use SqlEstoque\Http\Requests\Medida\UpdateMedida;

class MedidasController extends Controller
{
    /**
     * Repositorio
     * @var MedidaRepository
     */
    private $repository;

    /**
     * Contrução da classe
     * @param MedidaRepository $repository
     */
    public function __construct(MedidaRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $medidas = $this->repository->todasMedidas($request['search']);
        return view('medidas.index', compact('medidas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('medidas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMedida $request)
    {
        $request = $request->all();

        $this->repository->store($request);

        return \Redirect::route('medidas.index')
        ->with('success', [
            'title' => 'Medida cadastrada',
            'message' => 'Medida cadastrada com sucesso'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medida = $this->repository->getById($id);
        return view('medidas.edit', compact('medida'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMedida $request, $id)
    {
        $request = $request->all();
        $this->repository->update($request, $id);

        return \Redirect::route('medidas.index')
        ->with('success', [
            'title' => 'Medida editada',
            'message' => 'Medida editada com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->destroy($id);

        return \Redirect::route('medidas.index')
        ->with('success', [
            'title' => 'Medida excluída',
            'message' => 'Medida excluída com sucesso'
        ]);
    }
}
