const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.copy('resources/assets/assets', 'public/dist');
mix.copy('resources/assets/bootstrap', 'public/bootstrap');
mix.copy('resources/assets/plugins', 'public/plugins');