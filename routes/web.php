<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'admin'], function () {
        Route::resource('usuarios', 'Usuario\UsuariosController');
        Route::resource('clientes', 'Cliente\ClientesController');
    });

    Route::group(['prefix' => 'cadastros'], function () {
        Route::resource('empresas', 'Empresa\EmpresasController');
        Route::resource('funcionarios', 'Funcionario\FuncionariosController');
        Route::resource('fornecedores', 'Fornecedor\FornecedoresController');
        Route::resource('medidas', 'Medida\MedidasController');
        Route::resource('rotulos', 'Rotulo\RotulosController');
        Route::resource('natureza-operacao', 'NaturezaOperacao\NaturezaOperacaoController');
        Route::resource('cargos', 'Cargo\CargosController');
    });



    Route::group(['prefix' => 'ajax'], function () {
        Route::get('cidades/{estadoId}', 'Cidade\CidadesController@getCidades');
    });







    Route::get('/', function () { return view('home.home'); })->name('home');

});


Auth::routes();

